package com.gohainapp.gnh.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NotificationResponseData {
    @SerializedName("id")
    @Expose
    public String id;

    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("notificator_for")
    @Expose
    public String notificator_for;

    @SerializedName("user_id")
    @Expose
    public String user_id;

    @SerializedName("is_read")
    @Expose
    public String is_read;

    @SerializedName("icon")
    @Expose
    public String icon;

    @SerializedName("created_date")
    @Expose
    public String created_date;

}
