package com.gohainapp.gnh.domain.interactors.impl;

import com.gohainapp.gnh.domain.executors.Executor;
import com.gohainapp.gnh.domain.executors.MainThread;
import com.gohainapp.gnh.domain.interactors.VideoConsultationRequestInteractor;
import com.gohainapp.gnh.domain.interactors.base.AbstractInteractor;
import com.gohainapp.gnh.domain.models.VideoConsultationRequestResponse;
import com.gohainapp.gnh.repository.AppRepositoryImpl;

public class VideoConsultationRequestInteractorImpl extends AbstractInteractor implements VideoConsultationRequestInteractor {

    AppRepositoryImpl mRepository;
    VideoConsultationRequestInteractor.Callback mCallback;
    String authorization;
    int doctor_id;
    String appointment_data;

    public VideoConsultationRequestInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String authorization, int doctor_id, String appointment_data) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.authorization = authorization;
        this.doctor_id = doctor_id;
        this.appointment_data = appointment_data;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onVideoConsultationRequestFail(errorMsg);
            }
        });
    }

    private void postMessage(VideoConsultationRequestResponse videoConsultationRequestResponse){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onVideoConsultationRequestSuccess(videoConsultationRequestResponse);
            }
        });
    }

    @Override
    public void run() {
        final VideoConsultationRequestResponse videoConsultationRequestResponse = mRepository.requestVideoConsultation(authorization, doctor_id, appointment_data);
        if (videoConsultationRequestResponse == null) {
            notifyError("Please Check Your Internet Connection");
        } else if(videoConsultationRequestResponse.status == 400) {
            notifyError(videoConsultationRequestResponse.messages.mobileNo);
        } else if(videoConsultationRequestResponse.error != null){
            notifyError("invalid_token");
        } else {
            postMessage(videoConsultationRequestResponse);
        }
    }
}
