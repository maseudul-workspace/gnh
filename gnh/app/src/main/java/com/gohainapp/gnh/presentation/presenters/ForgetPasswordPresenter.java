package com.gohainapp.gnh.presentation.presenters;

public interface ForgetPasswordPresenter {
    void generateOtpReset(String phone);
    void checkOtp(String mobile, String otp);
    void changePassword(String phone, String password, String password_confirm);
    interface View {
        void onOtpSendSuccess();
        void onOtpCheckSuccess();
        void onChangePasswordSuccess();
        void showLoader();
        void hideLoader();
    }
}
