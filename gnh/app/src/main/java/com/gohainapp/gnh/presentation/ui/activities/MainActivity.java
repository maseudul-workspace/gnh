package com.gohainapp.gnh.presentation.ui.activities;

import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.gohainapp.gnh.R;
import com.gohainapp.gnh.domain.executors.impl.ThreadExecutor;
import com.gohainapp.gnh.domain.models.Banner;
import com.gohainapp.gnh.presentation.presenters.HomeActivityPresenter;
import com.gohainapp.gnh.presentation.presenters.impl.HomeActivityPresenterImpl;
import com.gohainapp.gnh.presentation.ui.adapters.HomeViewPagerAdapter;
import com.gohainapp.gnh.threading.MainThreadImpl;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseActivity implements HomeActivityPresenter.View {

    @BindView(R.id.btn_floatingAction)
    ExtendedFloatingActionButton btn_floating;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.dots_indicator)
    DotsIndicator dotsIndicator;
    HomeActivityPresenterImpl mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inflateContent(R.layout.activity_main);
        ButterKnife.bind(this);
        initialisePresenter();
        mPresenter.fetchBanners();
        btn_floating.shrink();
    }

    private void initialisePresenter() {
        mPresenter = new HomeActivityPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

        @OnClick(R.id.video_linear_layout) void  onBookVideoClicked () {
        Intent intent = new Intent(this, BookVideoConslutationActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.medicine_linear_layout) void  onBookMedicineClicked () {
        Intent intent = new Intent(this, RequestMedicineActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.lab_linear_layout) void  onBookLabTestClicked () {
        Intent intent = new Intent(this, RequestLabTestActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.btn_floatingAction) void onFloatClicked()
    {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:+919085511156"));
        startActivity(intent);
    }

    @Override
    public void loadBanners(Banner[] banners) {
        HomeViewPagerAdapter viewPagerAdapter =  new HomeViewPagerAdapter(this, banners);
        viewPager.setAdapter(viewPagerAdapter);
        dotsIndicator.setViewPager(viewPager);
    }
}