package com.gohainapp.gnh.presentation.presenters.impl;

import android.content.Context;

import com.gohainapp.gnh.AndroidApplication;
import com.gohainapp.gnh.domain.executors.Executor;
import com.gohainapp.gnh.domain.executors.MainThread;
import com.gohainapp.gnh.domain.interactors.FetchNotificationInteractor;
import com.gohainapp.gnh.domain.interactors.impl.FetchNotificationInteractorImpl;
import com.gohainapp.gnh.domain.models.LoginResponse;
import com.gohainapp.gnh.domain.models.NotificationResponseData;
import com.gohainapp.gnh.presentation.presenters.NotificatonPresenter;
import com.gohainapp.gnh.presentation.presenters.base.AbstractPresenter;
import com.gohainapp.gnh.presentation.ui.adapters.NotificationListRecyclerViewAdapter;
import com.gohainapp.gnh.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class NotificationPresenterImpl  extends AbstractPresenter implements NotificatonPresenter, FetchNotificationInteractor.Callback {

    Context mContext;
    NotificatonPresenter.View mView;
    NotificationResponseData[] newNotifications;
    NotificationListRecyclerViewAdapter adapter;

    public NotificationPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchNotifications(int offset, int limit) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        LoginResponse userInfo = androidApplication.getUserInfo(mContext);
        FetchNotificationInteractorImpl fetchNotificationInteractor = new FetchNotificationInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.access_token, offset, limit);
        fetchNotificationInteractor.execute();
        mView.showLoader();
    }

    @Override
    public void onNotificaionFetchSuccess(NotificationResponseData[] notifications, int total) {
        NotificationResponseData[] tempNotifications;
        tempNotifications = newNotifications;
        try {
            int len1 = tempNotifications.length;
            int len2 = notifications.length;
            newNotifications = new NotificationResponseData[len1 + len2];
            System.arraycopy(tempNotifications, 0, newNotifications, 0, len1);
            System.arraycopy(notifications, 0, newNotifications, len1, len2);
            adapter.updateData(newNotifications);
        }catch (NullPointerException e){
            newNotifications = notifications;
            adapter = new NotificationListRecyclerViewAdapter(mContext, notifications);
            mView.loadNotificationAdapter(adapter, total);
        }
        mView.hideLoader();
    }

    @Override
    public void onNotificaionFetchFail(String errorMsg) {
        mView.hideLoader();
        if(errorMsg.equals("invalid_token")) {
            mView.loginError();
            AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
            androidApplication.setUserInfo(mContext, null);
        } else {
            Toasty.warning(mContext, errorMsg).show();
        }
    }
}
