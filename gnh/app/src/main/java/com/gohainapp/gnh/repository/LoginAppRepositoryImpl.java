package com.gohainapp.gnh.repository;

import com.gohainapp.gnh.domain.models.LoginResponse;
import com.google.gson.Gson;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class LoginAppRepositoryImpl {

    LoginAppRepository mRepository;

    public LoginAppRepositoryImpl() {
        mRepository = LoginApiClient.createService(LoginAppRepository.class);
    }

    public LoginResponse checkLogin(String grantType, String username, String password) {
        LoginResponse loginResponse;
        String responseBody = "";
        Gson gson = new Gson();
        try {
            Call<ResponseBody> checkLogin = mRepository.checkLogin(grantType, username, password);
            Response<ResponseBody> response = checkLogin.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (!responseBody.isEmpty()) {
                loginResponse = gson.fromJson(responseBody, LoginResponse.class);
            } else {
                loginResponse = null;
            }
        }catch (Exception e){
            loginResponse = null;
        }
        return loginResponse;
    }
}
