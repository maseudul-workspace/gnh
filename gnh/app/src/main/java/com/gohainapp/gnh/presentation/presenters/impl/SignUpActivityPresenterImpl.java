package com.gohainapp.gnh.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.gohainapp.gnh.domain.executors.Executor;
import com.gohainapp.gnh.domain.executors.MainThread;
import com.gohainapp.gnh.domain.interactors.CheckOtpInteractor;
import com.gohainapp.gnh.domain.interactors.GenerateOtpInteractor;
import com.gohainapp.gnh.domain.interactors.impl.CheckOtpInteractorImpl;
import com.gohainapp.gnh.domain.interactors.impl.GenerateOtpInteractorImpl;
import com.gohainapp.gnh.presentation.presenters.SignUpActivityPresenter;
import com.gohainapp.gnh.presentation.presenters.base.AbstractPresenter;
import com.gohainapp.gnh.repository.AppRepositoryImpl;

public class SignUpActivityPresenterImpl extends AbstractPresenter implements SignUpActivityPresenter, GenerateOtpInteractor.Callback, CheckOtpInteractor.Callback {

    Context mContext;
    SignUpActivityPresenter.View mView;

    public SignUpActivityPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void generateOtp(String phone) {
        GenerateOtpInteractorImpl generateOtpInteractorImpl = new GenerateOtpInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, phone);
        generateOtpInteractorImpl.execute();
        mView.showLoader();
    }

    @Override
    public void checkOtp(String mobile, String otp) {
        CheckOtpInteractorImpl checkOtpInteractorImpl = new CheckOtpInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, mobile, otp);
        checkOtpInteractorImpl.execute();
        mView.showLoader();
    }

    @Override
    public void onGenerateOtpSuccess() {
        mView.hideLoader();
        mView.onOtpSendSuccess();
    }

    @Override
    public void onGenerateOtpFailed(String errorMsg) {
        mView.hideLoader();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onCheckOtpSuccess() {
        mView.hideLoader();
        mView.onOtpCheckSuccess();
    }

    @Override
    public void onCheckOtpFail(String errorMsg) {
        mView.hideLoader();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_LONG).show();
    }
}
