package com.gohainapp.gnh.presentation.presenters;

public interface MedicineRequestPresenter {
    void sendBookingRequest(int type, String medicine_desc, String filepath);
    interface View {
        void onMedicineRequestSuccess();
        void showLoader();
        void hideLoader();
        void loginError();
    }
}
