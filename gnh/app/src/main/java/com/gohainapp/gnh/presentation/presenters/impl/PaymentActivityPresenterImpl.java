package com.gohainapp.gnh.presentation.presenters.impl;

import android.content.Context;

import com.gohainapp.gnh.AndroidApplication;
import com.gohainapp.gnh.domain.executors.Executor;
import com.gohainapp.gnh.domain.executors.MainThread;
import com.gohainapp.gnh.domain.interactors.OrderByRequestInteractor;
import com.gohainapp.gnh.domain.interactors.VerifyPaymentInteractor;
import com.gohainapp.gnh.domain.interactors.impl.OrderByRequestInteractorImpl;
import com.gohainapp.gnh.domain.interactors.impl.VerifyPaymentInteractorImpl;
import com.gohainapp.gnh.domain.models.LoginResponse;
import com.gohainapp.gnh.domain.models.OrderByRequestResponse;
import com.gohainapp.gnh.presentation.presenters.PaymentActivityPresenter;
import com.gohainapp.gnh.presentation.presenters.base.AbstractPresenter;
import com.gohainapp.gnh.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class PaymentActivityPresenterImpl extends AbstractPresenter implements PaymentActivityPresenter,
        OrderByRequestInteractor.Callback,
        VerifyPaymentInteractor.Callback
{
    Context mContext;
    View mView;

    public PaymentActivityPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void onGetOrderByRequestSuccess(OrderByRequestResponse orderByRequestResponse) {
        mView.hideLoader();
        mView.loadGetOrderResponse(orderByRequestResponse);
    }

    @Override
    public void onGetOrderByRequestFail(String errorMsg) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }

    @Override
    public void getOrderByRequest(int id, int type) {
        OrderByRequestInteractorImpl orderByRequestInteractorimpl = new OrderByRequestInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, id, type);
        orderByRequestInteractorimpl.execute();
        mView.showLoader();
    }

    @Override
    public void verifyPayment(String razorpayOrderId, String razorpayPaymentId, String razorpaySignature) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        LoginResponse userInfo = androidApplication.getUserInfo(mContext);
        VerifyPaymentInteractorImpl verifyPaymentInteractor = new VerifyPaymentInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.access_token, razorpayOrderId, razorpayPaymentId, razorpaySignature);
        verifyPaymentInteractor.execute();
    }

    @Override
    public void onVerifyPaymentSuccess() {
        mView.goToPaymentSuccessActivity(true);
    }

    @Override
    public void onVerifyPaymentFailed(String errorMsg) {
        mView.goToPaymentSuccessActivity(false);
    }
}
