package com.gohainapp.gnh.presentation.ui.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.method.ScrollingMovementMethod;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.gohainapp.gnh.R;
import com.gohainapp.gnh.domain.executors.impl.ThreadExecutor;
import com.gohainapp.gnh.presentation.presenters.LabRequestPresenter;
import com.gohainapp.gnh.presentation.presenters.impl.LabRequestPresenterImpl;
import com.gohainapp.gnh.threading.MainThreadImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

import static com.gohainapp.gnh.util.Helper.calculateFileSize;
import static com.gohainapp.gnh.util.Helper.getRealPathFromURI;
import static com.gohainapp.gnh.util.Helper.saveImage;

public class RequestLabTestActivity extends AppCompatActivity implements LabRequestPresenter.View {

    String[] appPremisions;
    private static final int PERMISSIONS_REQUEST_CODE = 1240;
    private static final int REQUEST_PIC = 1000;
    String filePath;

    @BindView(R.id.edit_text_description)
    EditText editTextDescription;
    @BindView(R.id.text_view_upload_prescription)
    TextView textViewUploadPrescription;
    @BindView(R.id.checkbox_disclaimer)
    CheckBox checkBoxDisclaimer;
    @BindView(R.id.btn_continue_booking)
    Button btnContinueBooking;
    LabRequestPresenterImpl mPresenter;
    ProgressDialog progressDialog;
    @BindView(R.id.txt_view_disclaimer)
    TextView txtViewDisclaimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_lab_test);
        ButterKnife.bind(this);
        initialisePresenter();
        setProgressDialog();
        checkDisclaimerChecked();
        txtViewDisclaimer.setMovementMethod(new ScrollingMovementMethod());
    }

    private void checkDisclaimerChecked() {
        checkBoxDisclaimer.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    btnContinueBooking.setEnabled(true);
                } else {
                    btnContinueBooking.setEnabled(false);
                }
            }
        });
    }

    private void setProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Its loading....");
        progressDialog.setTitle("Please Wait");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    private void initialisePresenter() {
        mPresenter = new LabRequestPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @OnClick(R.id.btn_back) void onBackClicked()
    {
        finish();
    }

    @OnClick(R.id.btn_continue_booking) void onRequestLabTestClicked(){
        if(editTextDescription.getText().toString().isEmpty()) {
            Toasty.warning(this,"Description field cannot be empty").show();
        } else {
            mPresenter.sendBookingRequest(2,editTextDescription.getText().toString(), filePath);
        }
    }

    @Override
    public void onLabRequestSuccess() {
        Intent intent = new Intent(this, BookingSuccessfullActivity.class);
        startActivity(intent);
    }

    private boolean checkAndRequestPermissions(){
        List<String> listPermissionsNeeded = new ArrayList<>();
        for(String perm: appPremisions){
            if(ContextCompat.checkSelfPermission(this, perm) != PackageManager.PERMISSION_GRANTED){
                listPermissionsNeeded.add(perm);
            }
        }
        if(!listPermissionsNeeded.isEmpty()){
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), PERMISSIONS_REQUEST_CODE);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSIONS_REQUEST_CODE) {
            HashMap<String, Integer> permissionResults = new HashMap<>();
            int deniedCount = 0;
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    permissionResults.put(permissions[i], grantResults[i]);
                    deniedCount++;
                }
            }

            if (deniedCount == 0) {
                loadImageChooser();
            } else {
                for (Map.Entry<String, Integer> entry : permissionResults.entrySet()) {
                    String permName = entry.getKey();
                    int permResult = entry.getValue();
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, permName)) {
                        this.showAlertDialog("", "This app needs read and write storage permissions",
                                "Yes, Grant permissions",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        checkAndRequestPermissions();
                                    }
                                },
                                "Cancel",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        Toast.makeText(getApplicationContext(), "Permissions are required to download file", Toast.LENGTH_SHORT).show();
                                    }
                                },
                                false);
                    } else {
                        this.showAlertDialog("", "You have denied some permissions. Allow all permissions to download file at [Setting] > [Permissions]",
                                "Go to settings",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                                Uri.fromParts("package", getPackageName(), null));
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                    }
                                },
                                "Cancel",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        Toast.makeText(getApplicationContext(), "Permissions are required to download file", Toast.LENGTH_SHORT).show();
                                    }
                                },
                                false);
                        break;
                    }
                }
            }

        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Result code is RESULT_OK only if the user selects an Image
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_PIC) {
            if (data.getData() != null) {
                filePath = getRealPathFromURI(data.getData(), this);
                if (calculateFileSize(filePath) > 10) {
                    Toast.makeText(this, "Image should be less than 5 mb", Toast.LENGTH_SHORT).show();
                } else {
                    String path = filePath;
                    String filename = path.substring(path.lastIndexOf("/")+1);
                    textViewUploadPrescription.setText(filename);
                }
            } else {
                Bitmap photo = (Bitmap) data.getExtras().get("data");
                filePath = saveImage(photo);
                if (calculateFileSize(filePath) > 10) {
                    Toast.makeText(this, "Image should be less than 5 mb", Toast.LENGTH_SHORT).show();
                } else {
                    String path = filePath;
                    String filename = path.substring(path.lastIndexOf("/")+1);
                    textViewUploadPrescription.setText(filename);
                }
            }
        }
    }

    @OnClick(R.id.btn_upload) void onUploadClicked() {
        if (Build.VERSION.SDK_INT>= Build.VERSION_CODES.TIRAMISU){
            appPremisions = new String[]{
                    Manifest.permission.READ_MEDIA_IMAGES,
            };
        }else {
            appPremisions = new String[]{
                    Manifest.permission.READ_EXTERNAL_STORAGE,
            };
        }
        if (checkAndRequestPermissions()) {
            loadImageChooser();
        }
    }

    private void loadImageChooser() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
//        if (cameraIntent.resolveActivity(getPackageManager()) != null) {
//            File photoFile = null;
//            try {
//                photoFile = createImageFile();
//                Log.d("Image Result", photoFile.toString());
//            } catch (IOException ex) {
//                Log.e("Image Result", ex.toString());
//                ex.printStackTrace();
//            }
//            if (photoFile != null) {
//                Uri photoURI = FileProvider.getUriForFile(this, getApplicationContext().getPackageName() + ".fileprovider", photoFile);
//                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
//            }
//        }
//
//        Intent chooser = new Intent(Intent.ACTION_CHOOSER);
//        chooser.putExtra(Intent.EXTRA_INTENT, galleryIntent);
//        chooser.putExtra(Intent.EXTRA_TITLE, "Select from:");
//
//        Intent[] intentArray = { cameraIntent };
//        chooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray);
        startActivityForResult(galleryIntent, REQUEST_PIC);
    }

    public AlertDialog showAlertDialog(
            String title, String msg, String positiveLabel,
            DialogInterface.OnClickListener positiveOnClick,
            String negativeLabel, DialogInterface.OnClickListener negativeOnClick,
            boolean isCancelable
    ){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setCancelable(isCancelable);
        builder.setMessage(msg);
        builder.setPositiveButton(positiveLabel, positiveOnClick);
        builder.setNegativeButton(negativeLabel, negativeOnClick);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        return alertDialog;
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void loginError() {
        Toast.makeText(this, "Your session has expired !!", Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
}