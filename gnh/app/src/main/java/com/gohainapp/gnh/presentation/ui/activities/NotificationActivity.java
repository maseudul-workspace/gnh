package com.gohainapp.gnh.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.gohainapp.gnh.R;
import com.gohainapp.gnh.domain.executors.impl.ThreadExecutor;
import com.gohainapp.gnh.presentation.presenters.NotificatonPresenter;
import com.gohainapp.gnh.presentation.presenters.impl.NotificationPresenterImpl;
import com.gohainapp.gnh.presentation.ui.adapters.NotificationListRecyclerViewAdapter;
import com.gohainapp.gnh.threading.MainThreadImpl;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NotificationActivity extends AppCompatActivity implements NotificatonPresenter.View {

    @BindView(R.id.recycler_view_message_list)
    RecyclerView recyclerViewNotifications;
    @BindView(R.id.no_notification_linear_layout)
    LinearLayout noNotification;
    int page = 0;
    int offset = 0;
    int limit = 10;
    NotificationPresenterImpl mPresenter;
    ProgressDialog progressDialog;
    int totalPage = 0;
    LinearLayoutManager layoutManager;
    Boolean isScrolling = false;
    Integer currentItems;
    Integer totalItems;
    Integer scrollOutItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        ButterKnife.bind(this);
        initialisePresenter();
        setProgressDialog();
        mPresenter.fetchNotifications(offset, limit);
    }

    private void initialisePresenter() {
        mPresenter = new NotificationPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Its loading....");
        progressDialog.setTitle("Please Wait");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void loadNotificationAdapter(NotificationListRecyclerViewAdapter adapter, int total) {
        this.totalPage = total/10;
        if(adapter.getItemCount()!=0) {
            noNotification.setVisibility(View.GONE);
            recyclerViewNotifications.setVisibility(View.VISIBLE);
        }
        layoutManager = new LinearLayoutManager(this);
        recyclerViewNotifications.setLayoutManager(layoutManager);
        recyclerViewNotifications.setAdapter(adapter);
        recyclerViewNotifications.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
                {
                    isScrolling= true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItems = layoutManager.getChildCount();
                totalItems  = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstCompletelyVisibleItemPosition();
                if(!recyclerView.canScrollVertically(1))
                {
                    if(page < totalPage) {
                        offset = offset + 10;
                        isScrolling = false;
                        page = page + 1;
                        mPresenter.fetchNotifications(offset, limit);
                    }
                }
            }
        });
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void loginError() {
        Toast.makeText(this, "Your session has expired !!", Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @OnClick(R.id.btn_back) void onBackClicked()
    {
        finish();
    }

}