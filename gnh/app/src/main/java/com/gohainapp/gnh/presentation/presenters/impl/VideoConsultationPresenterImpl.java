package com.gohainapp.gnh.presentation.presenters.impl;

import android.content.Context;

import com.gohainapp.gnh.AndroidApplication;
import com.gohainapp.gnh.domain.executors.Executor;
import com.gohainapp.gnh.domain.executors.MainThread;
import com.gohainapp.gnh.domain.interactors.DoctorListInteractor;
import com.gohainapp.gnh.domain.interactors.VideoConsultationRequestInteractor;
import com.gohainapp.gnh.domain.interactors.impl.DoctorListInteractorImpl;
import com.gohainapp.gnh.domain.interactors.impl.VideoConsultationRequestInteractorImpl;
import com.gohainapp.gnh.domain.models.DoctorsList;
import com.gohainapp.gnh.domain.models.LoginResponse;
import com.gohainapp.gnh.domain.models.VideoConsultationRequestResponse;
import com.gohainapp.gnh.presentation.presenters.VideoConsultationPresenter;
import com.gohainapp.gnh.presentation.presenters.base.AbstractPresenter;
import com.gohainapp.gnh.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class VideoConsultationPresenterImpl extends AbstractPresenter implements VideoConsultationPresenter, DoctorListInteractor.Callback, VideoConsultationRequestInteractor.Callback {

    Context mContext;
    View mView;

    public VideoConsultationPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void onDoctorsListFetchSuccess(DoctorsList[] doctors) {
        mView.hideLoader();
        mView.loadDoctorsList(doctors);
    }

    @Override
    public void onDoctorsListFetchFail(String errorMsg) {
        mView.hideLoader();
        if(errorMsg.equals("invalid_token")) {
            mView.loginError();
            AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
            androidApplication.setUserInfo(mContext, null);
        } else {
            Toasty.warning(mContext, errorMsg).show();
        }
    }

    @Override
    public void fetchDoctors() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        LoginResponse userInfo = androidApplication.getUserInfo(mContext);
        DoctorListInteractorImpl doctorListInteractorImpl = new DoctorListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.access_token);
        doctorListInteractorImpl.execute();
        mView.showLoader();
    }

    @Override
    public void sendBookingRequest(int doctor_id, String appointment_date) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        LoginResponse userInfo = androidApplication.getUserInfo(mContext);
        VideoConsultationRequestInteractorImpl videoConsultationRequestInteractorImpl = new VideoConsultationRequestInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.access_token, doctor_id, appointment_date);
        videoConsultationRequestInteractorImpl.execute();
        mView.showLoader();
    }

    @Override
    public void onVideoConsultationRequestSuccess(VideoConsultationRequestResponse videoConsultationRequestResponse) {
        mView.hideLoader();
        mView.onBookingRequestSuccess();
    }

    @Override
    public void onVideoConsultationRequestFail(String errorMsg) {
        mView.hideLoader();
        if(errorMsg.equals("invalid_token")) {
            mView.loginError();
            AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
            androidApplication.setUserInfo(mContext, null);
        } else {
            Toasty.warning(mContext, errorMsg).show();
        }
    }

}
