package com.gohainapp.gnh.domain.interactors.impl;

import com.gohainapp.gnh.domain.executors.Executor;
import com.gohainapp.gnh.domain.executors.MainThread;
import com.gohainapp.gnh.domain.interactors.FetchNotificationInteractor;
import com.gohainapp.gnh.domain.interactors.base.AbstractInteractor;
import com.gohainapp.gnh.domain.models.NotificationResponseData;
import com.gohainapp.gnh.domain.models.NotificationResponseWrapper;
import com.gohainapp.gnh.repository.AppRepositoryImpl;

public class FetchNotificationInteractorImpl extends AbstractInteractor implements FetchNotificationInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String authorization;
    int offset;
    int limit;

    public FetchNotificationInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String authorization, int offset, int limit) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.authorization = authorization;
        this.offset = offset;
        this.limit = limit;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onNotificaionFetchFail(errorMsg);
            }
        });
    }

    private void postMessage(NotificationResponseData[] notificationResponseData, int total){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onNotificaionFetchSuccess(notificationResponseData, total);
            }
        });
    }

    @Override
    public void run() {
        final NotificationResponseWrapper notificationResponseWrapper = mRepository.fetchNotifications(authorization, offset, limit);
        if (notificationResponseWrapper == null) {
            notifyError("Please Check Your Internet Connection");
        } else if (notificationResponseWrapper.error != null) {
            notifyError("invalid_token");
        } else {
            postMessage( notificationResponseWrapper.notificationResponseData, notificationResponseWrapper.totalNotificationData.total );
        }
    }
}
