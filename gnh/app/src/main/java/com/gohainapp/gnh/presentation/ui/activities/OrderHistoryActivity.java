package com.gohainapp.gnh.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.gohainapp.gnh.R;
import com.gohainapp.gnh.presentation.ui.fragment.OrderHistoryFragment;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class OrderHistoryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_history);
        ButterKnife.bind(this);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                new OrderHistoryFragment()).commit();
    }

    @OnClick(R.id.btn_back) void onBackClicked()
    {
        finish();
    }
}