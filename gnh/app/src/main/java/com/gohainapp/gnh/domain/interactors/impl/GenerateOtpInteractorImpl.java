package com.gohainapp.gnh.domain.interactors.impl;

import com.gohainapp.gnh.domain.executors.Executor;
import com.gohainapp.gnh.domain.executors.MainThread;
import com.gohainapp.gnh.domain.interactors.GenerateOtpInteractor;
import com.gohainapp.gnh.domain.interactors.base.AbstractInteractor;
import com.gohainapp.gnh.domain.models.CommonResponse;
import com.gohainapp.gnh.repository.AppRepositoryImpl;

public class GenerateOtpInteractorImpl extends AbstractInteractor implements GenerateOtpInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String mobile;

    public GenerateOtpInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String mobile) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.mobile = mobile;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGenerateOtpFailed(errorMsg);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGenerateOtpSuccess();
            }
        });
    }

    @Override
    public void run() {
        final CommonResponse commonResponse = mRepository.generateOtp(mobile);
        if (commonResponse == null) {
            notifyError("Please Check Your Internet Connection");
        } else if (commonResponse.status == 400) {
            notifyError(commonResponse.messages.mobileNo);
        } else {
            postMessage();
        }
    }
}
