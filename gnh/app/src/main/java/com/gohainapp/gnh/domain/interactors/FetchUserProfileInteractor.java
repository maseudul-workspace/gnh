package com.gohainapp.gnh.domain.interactors;


import com.gohainapp.gnh.domain.models.UserInfoWrapper;

public interface FetchUserProfileInteractor {
    interface Callback {
        void onUserProfileFetchSuccess(UserInfoWrapper userInfoWrapper);
        void onUserProfileFetchFail(String errorMsg);
    }
}
