package com.gohainapp.gnh.domain.interactors;

import com.gohainapp.gnh.domain.models.UserInfoWrapper;

public interface ChangePasswordInteractor {
    interface Callback {
        void onChangePasswordSuccess(UserInfoWrapper userInfo);
        void onChangePasswordFail(String errorMsg);
    }
}
