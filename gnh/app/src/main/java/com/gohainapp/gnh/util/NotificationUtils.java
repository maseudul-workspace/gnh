//package com.webinfotech.easyparcel.util;
//
//import android.app.Notification;
//import android.app.NotificationChannel;
//import android.app.NotificationManager;
//import android.app.PendingIntent;
//import android.content.Context;
//import android.content.Intent;
//import android.graphics.Color;
//import android.net.Uri;
//import android.os.Build;
//import android.util.Log;
//import android.widget.ImageView;
//import android.widget.RemoteViews;
//
//import com.webinfotech.dbassociates.R;
////import com.webinfotech.dbassociates.domain.models.NotificationModel;
//
//import java.util.Random;
//
//import androidx.core.app.NotificationCompat;
//import androidx.core.app.NotificationManagerCompat;
//import androidx.core.content.ContextCompat;
//
//
///**
// * Created by Raj on 29-06-2019.
// */
//
//public class NotificationUtils {
//    private static final String CHANNEL_ID = "myChannel";
//    private static final String CHANNEL_NAME = "myChannelName";
//    private Context mContext;
//
//    public NotificationUtils(Context mContext) {
//        this.mContext = mContext;
//    }
//
//    public void displayNotification(NotificationModel model, Intent resultIntent){
//        String desc = model.desc;
//        String title = model.title;
//        PendingIntent resultPendingIntent;
//        Random r = new Random();
//        int app_Id = r.nextInt();
//
//        resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        resultPendingIntent = PendingIntent.getActivity(
//                                mContext,
//                                app_Id,
//                                resultIntent,
//                                PendingIntent.FLAG_UPDATE_CURRENT
//                        );
//
//        Notification notification = new NotificationCompat.Builder(mContext, CHANNEL_ID)
//                                    .setContentTitle(title)
//                                    .setContentText(title)
//                                    .setSmallIcon(R.drawable.parcel_blue)
//                                    .setColor(ContextCompat.getColor(mContext, R.color.colorPrimary))
//                                    .setStyle(new NotificationCompat.BigTextStyle()
//                                            .bigText(desc)
//                                            .setBigContentTitle(title)
//                                    )
//                                    .setPriority(Notification.PRIORITY_MAX)
//                                    .setContentIntent(resultPendingIntent)
//                                    .setAutoCancel(true)
//                                    .build();
//
//
//        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
//
//        //All notifications should go through NotificationChannel on Android 26 & above
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
//                    CHANNEL_NAME,
//                    NotificationManager.IMPORTANCE_DEFAULT);
//            notificationManager.createNotificationChannel(channel);
//
//        }
//        notificationManager.notify(app_Id, notification);
//
//    }
//
//
//
//
//}
