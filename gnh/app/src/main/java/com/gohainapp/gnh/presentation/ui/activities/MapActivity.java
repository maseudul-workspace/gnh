package com.gohainapp.gnh.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.gohainapp.gnh.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.getUiSettings().setZoomControlsEnabled(false);
        /* Enable my location button.*/
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        /* Enable Compass icon.*/
        googleMap.getUiSettings().setCompassEnabled(true);
        /* Enable Rotate gesture.*/
        googleMap.getUiSettings().setRotateGesturesEnabled(true);
        /* Enable zooming functionality.*/
        googleMap.getUiSettings().setZoomGesturesEnabled(true);
        LatLng latLng = new LatLng(26.356349,92.68861);
        googleMap.addMarker(new MarkerOptions().position(latLng).title("Gohain Nursing Home"));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15.0f));
        googleMap.getUiSettings().setMapToolbarEnabled(true);
    }
}