package com.gohainapp.gnh.presentation.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.gohainapp.gnh.R;
import com.gohainapp.gnh.presentation.ui.adapters.OrderHistoryFragmentAdapter;
import com.google.android.material.tabs.TabLayout;


import org.jetbrains.annotations.NotNull;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderHistoryFragment extends Fragment {

    @BindView(R.id.tab_layout)
    TabLayout tabLayout;
    @BindView(R.id.view_pager)
    ViewPager viewPager;
    OrderHistoryFragmentAdapter homeFragmentsAdapter;

    @Nullable
    @org.jetbrains.annotations.Nullable
    @Override
    public View onCreateView(@NonNull @NotNull LayoutInflater inflater, @Nullable @org.jetbrains.annotations.Nullable ViewGroup container, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_order_history, container, false);
        ButterKnife.bind(this,view);

        homeFragmentsAdapter = new OrderHistoryFragmentAdapter(getChildFragmentManager());
        viewPager.setAdapter(homeFragmentsAdapter);
        tabLayout.setupWithViewPager(viewPager);

        return view;
    }
}
