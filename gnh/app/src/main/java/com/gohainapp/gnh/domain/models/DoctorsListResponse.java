package com.gohainapp.gnh.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DoctorsListResponse {
    @SerializedName("data")
    @Expose
    public DoctorsList[] doctorsList;

    @SerializedName("error")
    @Expose
    public String error;

    @SerializedName("error_description")
    @Expose
    public String error_description;
}
