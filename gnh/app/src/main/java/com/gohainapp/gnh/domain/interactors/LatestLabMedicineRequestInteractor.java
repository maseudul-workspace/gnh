package com.gohainapp.gnh.domain.interactors;

import com.gohainapp.gnh.domain.models.RequestLabMedicineHistoryData;

public interface LatestLabMedicineRequestInteractor {
    interface Callback {
        void onLatestLabMedicineRequestFetchSuccess(RequestLabMedicineHistoryData[] requestLabMedicineHistories);
        void onLatestLabMedicineRequestFetchFail(String errorMsg);
    }
}
