package com.gohainapp.gnh.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Messages {

    @SerializedName("mobileNo")
    @Expose
    public String mobileNo;

    @SerializedName("medicine_desc")
    @Expose
    public String medicine_desc;

    @SerializedName("type")
    @Expose
    public String type;

    @SerializedName("phone")
    @Expose
    public String phone;

    @SerializedName("error")
    @Expose
    public String error;


    @SerializedName("doctor_id")
    @Expose
    public String doctor_id;

    @SerializedName("appointment_time")
    @Expose
    public String appointment_time;

    @SerializedName("opt")
    @Expose
    public String opt;

}
