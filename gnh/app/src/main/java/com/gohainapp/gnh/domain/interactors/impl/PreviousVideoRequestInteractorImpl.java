package com.gohainapp.gnh.domain.interactors.impl;

import com.gohainapp.gnh.domain.executors.Executor;
import com.gohainapp.gnh.domain.executors.MainThread;
import com.gohainapp.gnh.domain.interactors.PreviousVideoRequestInteractor;
import com.gohainapp.gnh.domain.interactors.base.AbstractInteractor;
import com.gohainapp.gnh.domain.models.RequestVideoHistoryData;
import com.gohainapp.gnh.domain.models.RequestVideoHistoryResponse;
import com.gohainapp.gnh.repository.AppRepositoryImpl;

public class PreviousVideoRequestInteractorImpl extends AbstractInteractor implements PreviousVideoRequestInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String authorization;

    public PreviousVideoRequestInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String authorization) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.authorization = authorization;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onPreviousVideoRequestFetchFail(errorMsg);
            }
        });
    }

    private void postMessage(RequestVideoHistoryData[] requestVideoHistories){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onPreviousVideoRequestFetchSuccess(requestVideoHistories);
            }
        });
    }

    @Override
    public void run() {
        final RequestVideoHistoryResponse requestVideoHistories = mRepository.previousVideoRequest(authorization);
        if (requestVideoHistories == null) {
            notifyError("Please Check Your Internet Connection");
        } else if(requestVideoHistories.error != null){
            notifyError("invalid_token");
        } else {
            postMessage(requestVideoHistories.requestVideoHistoryData);
        }
    }
}
