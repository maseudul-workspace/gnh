package com.gohainapp.gnh.presentation.presenters.impl;

import android.content.Context;

import com.gohainapp.gnh.AndroidApplication;
import com.gohainapp.gnh.domain.executors.Executor;
import com.gohainapp.gnh.domain.executors.MainThread;
import com.gohainapp.gnh.domain.interactors.FetchBannerInteractor;
import com.gohainapp.gnh.domain.interactors.impl.FetchBannerInteractorImpl;
import com.gohainapp.gnh.domain.models.Banner;
import com.gohainapp.gnh.domain.models.LoginResponse;
import com.gohainapp.gnh.presentation.presenters.HomeActivityPresenter;
import com.gohainapp.gnh.presentation.presenters.base.AbstractPresenter;
import com.gohainapp.gnh.repository.AppRepositoryImpl;

public class HomeActivityPresenterImpl extends AbstractPresenter implements HomeActivityPresenter,
         FetchBannerInteractor.Callback
{
    Context mContext;
    View mView;

    public HomeActivityPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void onBannerFetchSuccess(Banner[] banners) {
        mView.hideLoader();
        mView.loadBanners(banners);
    }

    @Override
    public void onBannerFetchFail(String errorMsg) {
        mView.hideLoader();
    }

    @Override
    public void fetchBanners() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        LoginResponse userInfo = androidApplication.getUserInfo(mContext);
        FetchBannerInteractorImpl fetchBannerInteractorImpl = new FetchBannerInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.access_token);
        fetchBannerInteractorImpl.execute();
        mView.showLoader();
    }
}
