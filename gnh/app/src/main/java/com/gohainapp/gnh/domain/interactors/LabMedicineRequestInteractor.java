package com.gohainapp.gnh.domain.interactors;

import com.gohainapp.gnh.domain.models.LabMedicineRequestResponse;

public interface LabMedicineRequestInteractor {
    interface Callback {
        void onLabMedicineRequestSuccess(LabMedicineRequestResponse labMedicineRequestResponse);
        void onLabMedicineRequestFail(String errorMsg);
    }
}
