package com.gohainapp.gnh.presentation.presenters;

import com.gohainapp.gnh.domain.models.OrderByRequestResponse;

public interface PaymentActivityPresenter {
    void getOrderByRequest(int id, int type);
    void verifyPayment(String razorpayOrderId, String razorpayPaymentId, String razorpaySignature);
    interface View {
        void loadGetOrderResponse(OrderByRequestResponse orderByRequestResponse);
        void goToPaymentSuccessActivity(boolean isSuccess);
        void showLoader();
        void hideLoader();
    }
}
