package com.gohainapp.gnh.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class UserInfoWrapper {

    @SerializedName("status")
    @Expose
    public int status;

    @SerializedName("messages")
    @Expose
    public Messages messages;

    @SerializedName("error")
    @Expose
    public String error;

    @SerializedName("id")
    @Expose
    public String id;

    @SerializedName("firstname")
    @Expose
    public String firstname;

    @SerializedName("lastname")
    @Expose
    public String lastname;

    @SerializedName("email")
    @Expose
    public String email;

    @SerializedName("phone")
    @Expose
    public String phone;

    @SerializedName("address")
    @Expose
    public String address;

    @SerializedName("pinCode")
    @Expose
    public String pinCode;

    @SerializedName("profile_pic")
    @Expose
    public String profile_pic;

}
