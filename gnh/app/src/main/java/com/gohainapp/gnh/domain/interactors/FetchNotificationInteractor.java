package com.gohainapp.gnh.domain.interactors;

import com.gohainapp.gnh.domain.models.NotificationResponseData;

public interface FetchNotificationInteractor {
    interface Callback {
        void onNotificaionFetchSuccess(NotificationResponseData[] notifications, int total);
        void onNotificaionFetchFail(String errorMsg);
    }
}
