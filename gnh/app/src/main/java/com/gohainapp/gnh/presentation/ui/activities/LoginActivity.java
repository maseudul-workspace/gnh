package com.gohainapp.gnh.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;

import com.gohainapp.gnh.R;
import com.gohainapp.gnh.domain.executors.impl.ThreadExecutor;
import com.gohainapp.gnh.presentation.presenters.LoginPresenter;
import com.gohainapp.gnh.presentation.presenters.impl.LoginPresenterImpl;
import com.gohainapp.gnh.threading.MainThreadImpl;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity implements LoginPresenter.View {

    LoginPresenterImpl mPresenter;
    @BindView(R.id.edit_text_mobile)
    EditText editTextMobile;
    @BindView(R.id.edit_text_password)
    EditText editTextPassword;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        initialisePresenter();
        setProgressDialog();
    }

    private void setProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Its loading....");
        progressDialog.setTitle("Please Wait");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    private void initialisePresenter() {
        mPresenter = new LoginPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @OnClick(R.id.btn_login) void onLoginClick() {
        mPresenter.chekLogin(editTextMobile.getText().toString(), editTextPassword.getText().toString());
    }

    @OnClick(R.id.btn_signUp) void onSignUpClick() {
        Intent intent = new Intent(this, SignUpActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.btn_forgetPassword) void onForgotPasswordClick() {
        Intent intent = new Intent(this, ForgotPasswordActivity.class);
        startActivity(intent);
    }


    @Override
    public void onLoginSuccess() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }
}