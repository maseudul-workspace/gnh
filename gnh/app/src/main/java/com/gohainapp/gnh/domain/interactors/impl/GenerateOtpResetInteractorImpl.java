package com.gohainapp.gnh.domain.interactors.impl;

import com.gohainapp.gnh.domain.executors.Executor;
import com.gohainapp.gnh.domain.executors.MainThread;
import com.gohainapp.gnh.domain.interactors.GenerateOtpResetInteractor;
import com.gohainapp.gnh.domain.interactors.base.AbstractInteractor;
import com.gohainapp.gnh.domain.models.CommonResponse;
import com.gohainapp.gnh.repository.AppRepositoryImpl;

public class GenerateOtpResetInteractorImpl extends AbstractInteractor implements GenerateOtpResetInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String mobile;

    public GenerateOtpResetInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String mobile) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.mobile = mobile;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGenerateOtpResetFailed(errorMsg);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGenerateOtpResetSuccess();
            }
        });
    }

    @Override
    public void run() {
        final CommonResponse commonResponse = mRepository.generateOtpReset(mobile);
        if (commonResponse == null) {
            notifyError("Please Check Your Internet Connection");
        } else if (commonResponse.status == 400) {
            notifyError(commonResponse.messages.mobileNo);
        } else {
            postMessage();
        }
    }
}
