package com.gohainapp.gnh.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.gohainapp.gnh.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class PrivacyPolicyActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy_policy);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btn_back) void onBackClicked()
    {
        finish();
    }
}