package com.gohainapp.gnh.presentation.presenters.impl;

import android.content.Context;

import com.gohainapp.gnh.AndroidApplication;
import com.gohainapp.gnh.domain.executors.Executor;
import com.gohainapp.gnh.domain.executors.MainThread;
import com.gohainapp.gnh.domain.interactors.LatestVideoRequestInteractor;
import com.gohainapp.gnh.domain.interactors.PreviousVideoRequestInteractor;
import com.gohainapp.gnh.domain.interactors.impl.LatestVideoRequestInteractorImpl;
import com.gohainapp.gnh.domain.interactors.impl.PreviousVideoRequestInteractorImpl;
import com.gohainapp.gnh.domain.models.RequestVideoHistoryData;
import com.gohainapp.gnh.domain.models.LoginResponse;
import com.gohainapp.gnh.presentation.presenters.VideoRequestHistoryPresenter;
import com.gohainapp.gnh.presentation.presenters.base.AbstractPresenter;
import com.gohainapp.gnh.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class VideoRequestHistoryPresenterImpl extends AbstractPresenter implements VideoRequestHistoryPresenter, LatestVideoRequestInteractor.Callback, PreviousVideoRequestInteractor.Callback {
    Context mContext;
    VideoRequestHistoryPresenter.View mView;

    public VideoRequestHistoryPresenterImpl(Executor executor, MainThread mainThread, Context mContext, VideoRequestHistoryPresenter.View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void onLatestVideoRequestFetchSuccess(RequestVideoHistoryData[] requestVideoHistories) {
        mView.hideLoader();
        mView.loadLatestRequest(requestVideoHistories);
    }

    @Override
    public void onPreviousVideoRequestFetchSuccess(RequestVideoHistoryData[] requestVideoHistories) {
        mView.hideLoader();
        mView.loadPreviousRequest(requestVideoHistories);
    }

    @Override
    public void onLatestVideoRequestFetchFail(String errorMsg) {
        mView.hideLoader();
        if(errorMsg.equals("invalid_token")) {
            mView.loginError();
            AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
            androidApplication.setUserInfo(mContext, null);
        } else {
            Toasty.warning(mContext, errorMsg).show();
        }
    }

    @Override
    public void onPreviousVideoRequestFetchFail(String errorMsg) {
        mView.hideLoader();
        if(errorMsg.equals("invalid_token")) {
            mView.loginError();
            AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
            androidApplication.setUserInfo(mContext, null);
        } else {
            Toasty.warning(mContext, errorMsg).show();
        }
    }

    @Override
    public void fetchLatestRequestVideoRequest() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        LoginResponse userInfo = androidApplication.getUserInfo(mContext);
        LatestVideoRequestInteractorImpl latestVideoRequestInteractorImpl = new LatestVideoRequestInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.access_token);
        latestVideoRequestInteractorImpl.execute();
        mView.showLoader();
    }

    @Override
    public void fetchPreviousRequestVideoRequest() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        LoginResponse userInfo = androidApplication.getUserInfo(mContext);
        PreviousVideoRequestInteractorImpl previousVideoRequestInteractorImpl = new PreviousVideoRequestInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.access_token);
        previousVideoRequestInteractorImpl.execute();
        mView.showLoader();
    }
}
