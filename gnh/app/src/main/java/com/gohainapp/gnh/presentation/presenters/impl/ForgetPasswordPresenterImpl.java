package com.gohainapp.gnh.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.gohainapp.gnh.domain.executors.Executor;
import com.gohainapp.gnh.domain.executors.MainThread;
import com.gohainapp.gnh.domain.interactors.ChangePasswordInteractor;
import com.gohainapp.gnh.domain.interactors.CheckOtpInteractor;
import com.gohainapp.gnh.domain.interactors.GenerateOtpResetInteractor;
import com.gohainapp.gnh.domain.interactors.impl.ChangePasswordInteractorImpl;
import com.gohainapp.gnh.domain.interactors.impl.CheckOtpInteractorImpl;
import com.gohainapp.gnh.domain.interactors.impl.GenerateOtpResetInteractorImpl;
import com.gohainapp.gnh.domain.models.UserInfoWrapper;
import com.gohainapp.gnh.presentation.presenters.ForgetPasswordPresenter;
import com.gohainapp.gnh.presentation.presenters.base.AbstractPresenter;
import com.gohainapp.gnh.repository.AppRepositoryImpl;

public class ForgetPasswordPresenterImpl extends AbstractPresenter implements ForgetPasswordPresenter, GenerateOtpResetInteractor.Callback, CheckOtpInteractor.Callback, ChangePasswordInteractor.Callback {

    Context mContext;
    ForgetPasswordPresenter.View mView;

    public ForgetPasswordPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void generateOtpReset(String phone) {
        GenerateOtpResetInteractorImpl generateOtpInteractorImpl = new GenerateOtpResetInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, phone);
        generateOtpInteractorImpl.execute();
        mView.showLoader();
    }

    @Override
    public void checkOtp(String mobile, String otp) {
        CheckOtpInteractorImpl checkOtpInteractorImpl = new CheckOtpInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, mobile, otp);
        checkOtpInteractorImpl.execute();
        mView.showLoader();
    }

    @Override
    public void changePassword(String phone, String password, String password_confirm) {
        ChangePasswordInteractorImpl changePasswordInteractorImpl = new ChangePasswordInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, phone, password, password_confirm);
        changePasswordInteractorImpl.execute();
        mView.showLoader();
    }

    @Override
    public void onCheckOtpSuccess() {
        mView.hideLoader();
        mView.onOtpCheckSuccess();
    }

    @Override
    public void onCheckOtpFail(String errorMsg) {
        mView.hideLoader();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onGenerateOtpResetSuccess() {
        mView.hideLoader();
        mView.onOtpSendSuccess();
    }

    @Override
    public void onGenerateOtpResetFailed(String errorMsg) {
        mView.hideLoader();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onChangePasswordSuccess(UserInfoWrapper userInfo) {
        mView.hideLoader();
        mView.onChangePasswordSuccess();
    }

    @Override
    public void onChangePasswordFail(String errorMsg) {
        mView.hideLoader();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_LONG).show();
    }
}
