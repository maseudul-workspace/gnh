package com.gohainapp.gnh.presentation.presenters;

import com.gohainapp.gnh.domain.models.RequestVideoHistoryData;

public interface VideoRequestHistoryPresenter {
    void fetchLatestRequestVideoRequest();
    void fetchPreviousRequestVideoRequest();
    interface View {
        void loadLatestRequest(RequestVideoHistoryData[] requestVideoHistories);
        void loadPreviousRequest(RequestVideoHistoryData[] requestVideoHistories);
        void showLoader();
        void hideLoader();
        void loginError();
    }
}
