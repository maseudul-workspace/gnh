package com.gohainapp.gnh.domain.interactors.impl;

import com.gohainapp.gnh.domain.executors.Executor;
import com.gohainapp.gnh.domain.executors.MainThread;
import com.gohainapp.gnh.domain.interactors.VerifyPaymentInteractor;
import com.gohainapp.gnh.domain.interactors.base.AbstractInteractor;
import com.gohainapp.gnh.domain.models.PaymentVerificationResponse;
import com.gohainapp.gnh.repository.AppRepositoryImpl;

public class VerifyPaymentInteractorImpl extends AbstractInteractor implements VerifyPaymentInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String authorization;
    String razorpayOrderId;
    String razorpayPaymentId;
    String razorpaySignature;

    public VerifyPaymentInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String authorization, String razorpayOrderId, String razorpayPaymentId, String razorpaySignature) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.authorization = authorization;
        this.razorpayOrderId = razorpayOrderId;
        this.razorpayPaymentId = razorpayPaymentId;
        this.razorpaySignature = razorpaySignature;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onVerifyPaymentFailed(errorMsg);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onVerifyPaymentSuccess();
            }
        });
    }

    @Override
    public void run() {
        final PaymentVerificationResponse paymentVerificationResponse = mRepository.verifyPayment(authorization, razorpayOrderId, razorpayPaymentId, razorpaySignature);
        if (paymentVerificationResponse == null) {
            notifyError("Please check your internet connection");
        } else if (paymentVerificationResponse.error != null) {
            notifyError(paymentVerificationResponse.error);
        } else {
            postMessage();
        }
    }
}
