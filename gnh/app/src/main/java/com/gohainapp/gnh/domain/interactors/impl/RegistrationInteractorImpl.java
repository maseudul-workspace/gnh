package com.gohainapp.gnh.domain.interactors.impl;

import com.gohainapp.gnh.domain.executors.Executor;
import com.gohainapp.gnh.domain.executors.MainThread;
import com.gohainapp.gnh.domain.interactors.RegistrationInteractor;
import com.gohainapp.gnh.domain.interactors.base.AbstractInteractor;
import com.gohainapp.gnh.domain.models.UserInfoWrapper;
import com.gohainapp.gnh.repository.AppRepositoryImpl;

public class RegistrationInteractorImpl extends AbstractInteractor implements RegistrationInteractor {

    AppRepositoryImpl mRepository;
    RegistrationInteractor.Callback mCallback;
    String firstname;
    String lastname;
    String email;
    String phone;
    String password;
    String password_confirm;
    String pinCode;
    String address;

    public RegistrationInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String firstname, String lastname, String phone, String email, String password, String password_confirm, String pinCode, String address) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.firstname = firstname;
        this.lastname = lastname;
        this.phone = phone;
        this.email = email;
        this.password = password;
        this.password_confirm = password_confirm;
        this.pinCode = pinCode;
        this.address = address;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onRegistrationFail(errorMsg);
            }
        });
    }

    private void postMessage(UserInfoWrapper userInfo){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onRegistrationSuccess(userInfo);
            }
        });
    }


    @Override
    public void run() {
        final UserInfoWrapper userInfoWrapper = mRepository.registration(firstname, lastname, email, phone, password, password_confirm, address, pinCode);
        if (userInfoWrapper == null) {
            notifyError("Please Check Your Internet Connection");
        } else if (userInfoWrapper.status == 400) {
            notifyError(userInfoWrapper.messages.phone);
        } else {
            postMessage(userInfoWrapper);
        }
    }
}
