package com.gohainapp.gnh.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.gohainapp.gnh.AndroidApplication;
import com.gohainapp.gnh.R;
import com.gohainapp.gnh.domain.executors.impl.ThreadExecutor;
import com.gohainapp.gnh.domain.models.UserInfoWrapper;
import com.gohainapp.gnh.presentation.presenters.ProfileActivitySettingsPresenter;
import com.gohainapp.gnh.presentation.presenters.impl.ProfileActivitySettingsPresenterImpl;
import com.gohainapp.gnh.threading.MainThreadImpl;
import com.gohainapp.gnh.util.GlideHelper;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProfileSettingsActivity extends AppCompatActivity implements ProfileActivitySettingsPresenter.View {

    @BindView(R.id.text_view_profile_settings_user_name)
    TextView textViewUserName;
    @BindView(R.id.text_view_profile_settings_user_mail)
    TextView textViewUserMail;
    @BindView(R.id.profile_settings_user_image)
    ImageView imageViewUserImage;
    ProfileActivitySettingsPresenterImpl mPresenter;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_settings);
        ButterKnife.bind(this);
        initialisePresenter();
        setProgressDialog();
        mPresenter.fetchUserDetails();
    }

    private void setProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Its loading....");
        progressDialog.setTitle("Please Wait");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    private void initialisePresenter() {
        mPresenter = new ProfileActivitySettingsPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @OnClick(R.id.material_card_account_settings) void onAccountSettingsClicked () {
        Intent intent = new Intent(this, ProfileActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.material_card_notification) void onNotificationClicked () {
        Intent intent = new Intent(this, NotificationActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.material_card_privacy_policy) void onPrivacyPolicyClicked () {
        Intent intent = new Intent(this, PrivacyPolicyActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.material_card_support) void onSupportClicked () {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:+919085511156"));
        startActivity(intent);
    }

    @OnClick(R.id.material_card_log_out) void onLogoutClicked() {
       mPresenter.Logout();
    }

    @OnClick(R.id.btn_back) void onBackClicked()
    {
        finish();
    }

    @Override
    public void onLogoutSuccess() {
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        androidApplication.setUserInfo(getApplicationContext(), null);
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void loadUserDetails(UserInfoWrapper userInfoWrapper) {
        if(userInfoWrapper.profile_pic!=null) {
            GlideHelper.setImageViewCustomRoundedCorners(this, imageViewUserImage ,getResources().getString(R.string.base_url) + userInfoWrapper.profile_pic,150);
        }
        textViewUserName.setText(capsName(userInfoWrapper.firstname)+" "+capsName(userInfoWrapper.lastname));
        textViewUserMail.setText(userInfoWrapper.email);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void loginError() {
        Toast.makeText(this, "Your session has expired !!", Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private StringBuilder capsName(String name)
    {
        StringBuilder res = new StringBuilder();
        try {
            String[] strArr = name.split(" ");
            for (String str : strArr) {
                char[] stringArray = str.trim().toCharArray();
                stringArray[0] = Character.toUpperCase(stringArray[0]);
                str = new String(stringArray);

                res.append(str).append(" ");
            }
            return res;
        } catch (Exception e) {
            return res.append("");
        }
    }
}
