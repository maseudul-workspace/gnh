package com.gohainapp.gnh.presentation.presenters;

public interface LoginPresenter {
    void chekLogin(String user, String password);
    interface View {
        void onLoginSuccess();
        void showLoader();
        void hideLoader();
    }
}