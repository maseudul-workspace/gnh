package com.gohainapp.gnh.presentation.ui.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.gohainapp.gnh.R;
import com.gohainapp.gnh.domain.executors.impl.ThreadExecutor;
import com.gohainapp.gnh.domain.models.RequestVideoHistoryData;
import com.gohainapp.gnh.presentation.presenters.VideoRequestHistoryPresenter;
import com.gohainapp.gnh.presentation.presenters.impl.VideoRequestHistoryPresenterImpl;
import com.gohainapp.gnh.presentation.ui.activities.BookVideoConslutationActivity;
import com.gohainapp.gnh.presentation.ui.activities.LoginActivity;
import com.gohainapp.gnh.presentation.ui.activities.PaymentActivity;
import com.gohainapp.gnh.presentation.ui.adapters.LatestVideoRequestItemsRecyclerViewAdapter;
import com.gohainapp.gnh.presentation.ui.adapters.PreviousVideoRequestItemsRecyclerViewAdapter;
import com.gohainapp.gnh.threading.MainThreadImpl;

import org.jetbrains.annotations.NotNull;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class VideoHistoryTabFragment extends Fragment implements VideoRequestHistoryPresenter.View, LatestVideoRequestItemsRecyclerViewAdapter.Callback, PreviousVideoRequestItemsRecyclerViewAdapter.Callback {

    @BindView(R.id.recycler_view_latest_history)
    RecyclerView recyclerViewLatestHistory;
    @BindView(R.id.recycler_view_previous_history)
    RecyclerView recyclerViewPreviousHistory;
    @BindView(R.id.text_view_latest_history)
    TextView textViewLatestHistory;
    @BindView(R.id.text_view_previous_history)
    TextView textViewPreviousHistory;
    @BindView(R.id.nested_scroll_view)
    NestedScrollView nestedScrollView;
    @BindView(R.id.linear_layout_no_history)
    LinearLayout linearLayoutNoHistory;
    @BindView(R.id.linear_layout_history)
    LinearLayout linearLayoutHistory;
    LatestVideoRequestItemsRecyclerViewAdapter latestVideoRequestItemsRecyclerViewAdapter;
    PreviousVideoRequestItemsRecyclerViewAdapter previousVideoRequestItemsRecyclerViewAdapter;
    VideoRequestHistoryPresenterImpl mPresenter;
    ProgressDialog progressDialog;

    @Nullable
    @org.jetbrains.annotations.Nullable
    @Override
    public View onCreateView(@NonNull @NotNull LayoutInflater inflater, @Nullable @org.jetbrains.annotations.Nullable ViewGroup container, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_video_history_tab, container, false);
        ButterKnife.bind(this,view);
        initialisePresenter();
        setProgressDialog();
        return view;
    }

    private void initialisePresenter() {
        mPresenter = new VideoRequestHistoryPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), getContext(), this);
    }

    private void setProgressDialog() {
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Its loading....");
        progressDialog.setTitle("Please Wait");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void loadLatestRequest(RequestVideoHistoryData[] requestVideoHistories) {
        if(requestVideoHistories.length != 0) {
            linearLayoutNoHistory.setVisibility(View.GONE);
            nestedScrollView.setVisibility(View.VISIBLE);
            textViewLatestHistory.setVisibility(View.VISIBLE);
        }
        recyclerViewLatestHistory.setLayoutManager(new LinearLayoutManager(getContext()));
        latestVideoRequestItemsRecyclerViewAdapter = new LatestVideoRequestItemsRecyclerViewAdapter(getContext(), requestVideoHistories, this);
        recyclerViewLatestHistory.setAdapter(latestVideoRequestItemsRecyclerViewAdapter);
        recyclerViewLatestHistory.setItemAnimator(null);
    }

    @Override
    public void loadPreviousRequest(RequestVideoHistoryData[] requestVideoHistories) {
        if(requestVideoHistories.length != 0) {
            linearLayoutNoHistory.setVisibility(View.GONE);
            nestedScrollView.setVisibility(View.VISIBLE);
            textViewPreviousHistory.setVisibility(View.VISIBLE);
        }
        recyclerViewPreviousHistory.setLayoutManager(new LinearLayoutManager(getContext()));
        previousVideoRequestItemsRecyclerViewAdapter = new PreviousVideoRequestItemsRecyclerViewAdapter(getContext(), requestVideoHistories, this);
        recyclerViewPreviousHistory.setAdapter(previousVideoRequestItemsRecyclerViewAdapter);
        recyclerViewPreviousHistory.setItemAnimator(null);
    }

    @OnClick(R.id.btn_order_now) void onOrderNowClicked () {
        Intent intent = new Intent(getContext(), BookVideoConslutationActivity.class);
        startActivity(intent);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void loginError() {
        Toast.makeText(getContext(), "Your session has expired !!", Toast.LENGTH_LONG).show();
        Intent intent = new Intent(getContext(), LoginActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

    @Override
    public void onPaymentClicked(int id, int type) {
        Intent intent = new Intent(getContext(), PaymentActivity.class);
        intent.putExtra("id", id);
        intent.putExtra("type", type);
        startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.fetchLatestRequestVideoRequest();
        mPresenter.fetchPreviousRequestVideoRequest();
    }

}
