package com.gohainapp.gnh.domain.interactors.impl;

import com.gohainapp.gnh.domain.executors.Executor;
import com.gohainapp.gnh.domain.executors.MainThread;
import com.gohainapp.gnh.domain.interactors.CheckOtpInteractor;
import com.gohainapp.gnh.domain.interactors.base.AbstractInteractor;
import com.gohainapp.gnh.domain.models.CommonResponse;
import com.gohainapp.gnh.repository.AppRepositoryImpl;

public class CheckOtpInteractorImpl extends AbstractInteractor implements CheckOtpInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String mobile;
    String otp;

    public CheckOtpInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String mobile, String otp) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.mobile = mobile;
        this.otp = otp;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onCheckOtpFail(errorMsg);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onCheckOtpSuccess();
            }
        });
    }

    @Override
    public void run() {
        final CommonResponse commonResponse = mRepository.checkOtp(mobile, otp);
        if (commonResponse == null) {
            notifyError("Please Check Your Internet Connection");
        } else if (commonResponse.status == 400) {
            notifyError(commonResponse.message);
        } else {
            if(commonResponse.message.equals("Otp match")) {
                postMessage();
            } else  {
                notifyError(commonResponse.message);

            }
        }
    }
}
