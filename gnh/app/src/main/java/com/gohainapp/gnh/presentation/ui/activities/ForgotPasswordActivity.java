package com.gohainapp.gnh.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.airbnb.lottie.LottieAnimationView;
import com.gohainapp.gnh.R;
import com.gohainapp.gnh.domain.executors.impl.ThreadExecutor;
import com.gohainapp.gnh.presentation.presenters.ForgetPasswordPresenter;
import com.gohainapp.gnh.presentation.presenters.impl.ForgetPasswordPresenterImpl;
import com.gohainapp.gnh.threading.MainThreadImpl;
import com.goodiebag.pinview.Pinview;
import com.google.android.material.textfield.TextInputLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

public class ForgotPasswordActivity extends AppCompatActivity implements ForgetPasswordPresenter.View {

    @BindView(R.id.edit_text_ForgetPass_Phone)
    EditText editForgetPassPhone;
    @BindView(R.id.edit_text_new_password)
    EditText editNewPassPhone;
    @BindView(R.id.edit_text_confirm_password)
    EditText editConfirmPassPhone;
    @BindView(R.id.pinview)
    Pinview pinview;
    @BindView(R.id.edit_text_input_forgetPass_phone_layout)
    TextInputLayout textInputForgetPassPhoneLayout;
    @BindView(R.id.resetPassLinearLayout)
    LinearLayout resetPassLinearLayout;
    @BindView(R.id.text_input_new_password_layout)
    TextInputLayout textNewPassLinearLayout;
    @BindView(R.id.text_input_confirm_password_layout)
    TextInputLayout textConfirmPassLinearLayout;
    @BindView(R.id.changePassLinearLayout)
    LinearLayout changePassLinearLayout;
    @BindView(R.id.otpVerifyLinearLayout)
    LinearLayout otpVerifyLinearLayout;
    LottieAnimationView lottieAnimationView;
    LottieAnimationView lottieAnimationView2;
    LottieAnimationView lottieAnimationView3;
    ForgetPasswordPresenterImpl mPresenter;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        ButterKnife.bind(this);
        initialisePresenter();
        setProgressDialog();
        lottieAnimationView = findViewById(R.id.forgetpass);
        lottieAnimationView.playAnimation();
        lottieAnimationView.loop(true);

    }

    private void setProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Its loading....");
        progressDialog.setTitle("Please Wait");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

        private void initialisePresenter() {
        mPresenter = new ForgetPasswordPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @OnClick(R.id.btn_send_otp) void onSendOtpClicked()
    {
        textInputForgetPassPhoneLayout.setError("");

        if(editForgetPassPhone.length()!=10) {
            textInputForgetPassPhoneLayout.setError("Insert Valid Phone Number");
        } else {
           mPresenter.generateOtpReset(editForgetPassPhone.getText().toString());
        }
    }

    @OnClick(R.id.btn_verify_otp) void onVerifyOtp() {
        if(pinview.getValue().length()!=4) {
            Toasty.warning(this, "Please enter correct OTP").show();
        } else {
            mPresenter.checkOtp(editForgetPassPhone.getText().toString(),pinview.getValue());
        }
    }

    @OnClick(R.id.btn_change_password) void onChangedPassClicked() {
        textNewPassLinearLayout.setError("");
        textConfirmPassLinearLayout.setError("");
         if(editNewPassPhone.getText().toString().isEmpty()) {
            textNewPassLinearLayout.setError("Please insert your password");
        } else if(editNewPassPhone.getText().toString().length() < 8) {
            textNewPassLinearLayout.setError("password must be at least 8 characters");
        } else if(!editConfirmPassPhone.getText().toString().equals(editNewPassPhone.getText().toString())) {
            textConfirmPassLinearLayout.setError("Those passwords didn't match. Try again");
        } else {
            mPresenter.changePassword(editForgetPassPhone.getText().toString(), editNewPassPhone.getText().toString(), editConfirmPassPhone.getText().toString());
        }
    }

    @Override
    public void onOtpSendSuccess() {
        resetPassLinearLayout.setVisibility(View.GONE);
        otpVerifyLinearLayout.setVisibility(View.VISIBLE);
        lottieAnimationView3 = findViewById(R.id.newpassotp);
        lottieAnimationView3.playAnimation();
        lottieAnimationView3.loop(true);
    }

    @Override
    public void onOtpCheckSuccess() {
        otpVerifyLinearLayout.setVisibility(View.GONE);
        changePassLinearLayout.setVisibility(View.VISIBLE);
        lottieAnimationView2 = findViewById(R.id.newpass);
        lottieAnimationView2.playAnimation();
        lottieAnimationView2.loop(true);
    }

    @Override
    public void onChangePasswordSuccess() {
        Toasty.success(this,"Password Changed Successfully",Toasty.LENGTH_SHORT).show();
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }
}