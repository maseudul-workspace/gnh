package com.gohainapp.gnh.domain.interactors.impl;

import com.gohainapp.gnh.domain.executors.Executor;
import com.gohainapp.gnh.domain.executors.MainThread;
import com.gohainapp.gnh.domain.interactors.GetUnreadNotificationInteractor;
import com.gohainapp.gnh.domain.interactors.base.AbstractInteractor;
import com.gohainapp.gnh.domain.models.TotalNotificationData;
import com.gohainapp.gnh.repository.AppRepositoryImpl;

public class GetUnreadNotificationInteractorImpl extends AbstractInteractor implements GetUnreadNotificationInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;

    public GetUnreadNotificationInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingUnreadNotificationFail(errorMsg);
            }
        });
    }

    private void postMessage(int total){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingUnreadNotificationSuccess(total);
            }
        });
    }

    @Override
    public void run() {
        TotalNotificationData totalNotificationData = mRepository.getUnreadNotification(apiToken);
        if (totalNotificationData == null) {
            notifyError("Please check your internet connection");
        } else if (totalNotificationData.error != null) {
            notifyError("invalid_token");
        } else {
            postMessage(totalNotificationData.total);
        }
    }
}
