package com.gohainapp.gnh.domain.interactors.impl;

import com.gohainapp.gnh.domain.executors.Executor;
import com.gohainapp.gnh.domain.executors.MainThread;
import com.gohainapp.gnh.domain.interactors.PreviousLabMedicineRequestInteractor;
import com.gohainapp.gnh.domain.interactors.base.AbstractInteractor;
import com.gohainapp.gnh.domain.models.RequestLabMedicineHistoryData;
import com.gohainapp.gnh.domain.models.RequestLabMedicineHistoryResponse;
import com.gohainapp.gnh.repository.AppRepositoryImpl;

public class PreviousLabMedicineRequestInteractorImpl extends AbstractInteractor implements PreviousLabMedicineRequestInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String authorization;
    int type;

    public PreviousLabMedicineRequestInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String authorization, int type) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.authorization = authorization;
        this.type = type;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onPreviousLabMedicineRequestFetchFail(errorMsg);
            }
        });
    }

    private void postMessage(RequestLabMedicineHistoryData[] requestLabMedicineHistories){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onPreviousLabMedicineRequestFetchSuccess(requestLabMedicineHistories);
            }
        });
    }

    @Override
    public void run() {
        final RequestLabMedicineHistoryResponse requestLabMedicineHistories = mRepository.previousLabMedicineRequest(authorization, type);
        if (requestLabMedicineHistories == null) {
            notifyError("Please Check Your Internet Connection");
        } else if(requestLabMedicineHistories.error != null){
            notifyError("invalid_token");
        } else {
            postMessage(requestLabMedicineHistories.requestLabMedicineHistoryData);
        }
    }
}
