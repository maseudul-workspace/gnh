package com.gohainapp.gnh.domain.interactors;

public interface CheckOtpInteractor {
    interface Callback {
        void onCheckOtpSuccess();
        void onCheckOtpFail(String errorMsg);
    }
}
