package com.gohainapp.gnh.domain.interactors.impl;

import com.gohainapp.gnh.domain.executors.Executor;
import com.gohainapp.gnh.domain.executors.MainThread;
import com.gohainapp.gnh.domain.interactors.OrderByRequestInteractor;
import com.gohainapp.gnh.domain.interactors.base.AbstractInteractor;
import com.gohainapp.gnh.domain.models.OrderByRequestResponse;
import com.gohainapp.gnh.repository.AppRepositoryImpl;

public class OrderByRequestInteractorImpl extends AbstractInteractor implements OrderByRequestInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    int id;
    int type;

    public OrderByRequestInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, int id, int type) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.id = id;
        this.type = type;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGetOrderByRequestFail(errorMsg);
            }
        });
    }

    private void postMessage(OrderByRequestResponse orderByRequestResponse){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGetOrderByRequestSuccess(orderByRequestResponse);
            }
        });
    }

    @Override
    public void run() {
        final OrderByRequestResponse orderByRequestResponse = mRepository.getOrderByRequest(id, type);
        if (orderByRequestResponse == null) {
            notifyError("Please Check Your Internet Connection");
        } else if (orderByRequestResponse.status == 400) {
            notifyError(orderByRequestResponse.messages.error);
        } else {
            postMessage(orderByRequestResponse);
        }
    }
}
