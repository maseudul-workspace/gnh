package com.gohainapp.gnh.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.airbnb.lottie.LottieAnimationView;
import com.gohainapp.gnh.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegistrationSuccessfullActivity extends AppCompatActivity {

    @BindView(R.id.lotte_success)
    LottieAnimationView lottieAnimationViewSuccess ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_successfull);
        ButterKnife.bind(this);
        lottieAnimationViewSuccess.playAnimation();
    }

    @OnClick(R.id.btn_login) void onLoginClicked() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
}