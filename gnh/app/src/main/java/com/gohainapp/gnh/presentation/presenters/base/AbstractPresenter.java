 package com.gohainapp.gnh.presentation.presenters.base;

import com.gohainapp.gnh.domain.executors.Executor;
import com.gohainapp.gnh.domain.executors.MainThread;

/**
 * Created by Raj on 05-01-2019.
 */

public abstract class AbstractPresenter {
    protected Executor mExecutor;
    protected MainThread mMainThread;
    public AbstractPresenter(Executor executor, MainThread mainThread){
        this.mExecutor = executor;
        this.mMainThread = mainThread;
    }
}
