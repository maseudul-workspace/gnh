package com.gohainapp.gnh.presentation.presenters;

import com.gohainapp.gnh.domain.models.UserInfoWrapper;

public interface BaseActivityPresenter {
    void Logout();
    void fetchUserDetails();
    void fetchNewNotification();
    interface View {
        void loadUserDetails(UserInfoWrapper userInfoWrapper);
        void loadNewNotificaction(int total);
        void onLogoutSuccess();
        void showLoader();
        void hideLoader();
        void loginError();
    }
}
