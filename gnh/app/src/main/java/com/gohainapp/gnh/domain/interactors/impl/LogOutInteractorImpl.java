package com.gohainapp.gnh.domain.interactors.impl;

import com.gohainapp.gnh.domain.executors.Executor;
import com.gohainapp.gnh.domain.executors.MainThread;
import com.gohainapp.gnh.domain.interactors.LogOutInteractor;
import com.gohainapp.gnh.domain.interactors.base.AbstractInteractor;
import com.gohainapp.gnh.domain.models.LogOutResponse;
import com.gohainapp.gnh.repository.AppRepositoryImpl;

public class LogOutInteractorImpl extends AbstractInteractor implements LogOutInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String token;

    public LogOutInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String token) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.token = token;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onLogOutFail(errorMsg);
            }
        });
    }

    private void postMessage(LogOutResponse logOutResponse){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onLogOutSuccess(logOutResponse);
            }
        });
    }


    @Override
    public void run() {
        final LogOutResponse logOutResponse = mRepository.logOut(token);
        if (logOutResponse == null) {
            notifyError("Please Check Your Internet Connection");
        } else if (logOutResponse.error != null) {
            notifyError(logOutResponse.error_description);
        } else {
            postMessage(logOutResponse);
        }
    }
}
