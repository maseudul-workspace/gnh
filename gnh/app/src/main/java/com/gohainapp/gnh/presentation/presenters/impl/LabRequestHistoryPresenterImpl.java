package com.gohainapp.gnh.presentation.presenters.impl;

import android.content.Context;

import com.gohainapp.gnh.AndroidApplication;
import com.gohainapp.gnh.domain.executors.Executor;
import com.gohainapp.gnh.domain.executors.MainThread;
import com.gohainapp.gnh.domain.interactors.LatestLabMedicineRequestInteractor;
import com.gohainapp.gnh.domain.interactors.PreviousLabMedicineRequestInteractor;
import com.gohainapp.gnh.domain.interactors.impl.LatestLabMedicineRequestInteractorImpl;
import com.gohainapp.gnh.domain.interactors.impl.PreviousLabMedicineRequestInteractorImpl;
import com.gohainapp.gnh.domain.models.LoginResponse;
import com.gohainapp.gnh.domain.models.RequestLabMedicineHistoryData;
import com.gohainapp.gnh.presentation.presenters.LabRequestHistoryPresenter;
import com.gohainapp.gnh.presentation.presenters.base.AbstractPresenter;
import com.gohainapp.gnh.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class LabRequestHistoryPresenterImpl extends AbstractPresenter implements LabRequestHistoryPresenter, LatestLabMedicineRequestInteractor.Callback, PreviousLabMedicineRequestInteractor.Callback {
    Context mContext;
    View mView;

    public LabRequestHistoryPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchLatestRequest(int type) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        LoginResponse userInfo = androidApplication.getUserInfo(mContext);
        LatestLabMedicineRequestInteractorImpl latestLabMedicineRequestInteractorImpl = new LatestLabMedicineRequestInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.access_token, type);
        latestLabMedicineRequestInteractorImpl.execute();
        mView.showLoader();
    }

    @Override
    public void fetchPreviousRequest(int type) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        LoginResponse userInfo = androidApplication.getUserInfo(mContext);
        PreviousLabMedicineRequestInteractorImpl previousLabMedicineRequestInteractorImpl = new PreviousLabMedicineRequestInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.access_token, type);
        previousLabMedicineRequestInteractorImpl.execute();
        mView.showLoader();
    }

    @Override
    public void onLatestLabMedicineRequestFetchSuccess(RequestLabMedicineHistoryData[] requestLabMedicineHistories) {
        mView.hideLoader();
        mView.loadLatestRequest(requestLabMedicineHistories);
    }

    @Override
    public void onLatestLabMedicineRequestFetchFail(String errorMsg) {
        mView.hideLoader();
        if(errorMsg.equals("invalid_token")) {
            mView.loginError();
            AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
            androidApplication.setUserInfo(mContext, null);
        } else {
            Toasty.warning(mContext, errorMsg).show();
        }
    }

    @Override
    public void onPreviousLabMedicineRequestFetchSuccess(RequestLabMedicineHistoryData[] requestLabMedicineHistories) {
        mView.hideLoader();
        mView.loadPreviousRequest(requestLabMedicineHistories);
    }

    @Override
    public void onPreviousLabMedicineRequestFetchFail(String errorMsg) {
        mView.hideLoader();
        if(errorMsg.equals("invalid_token")) {
            mView.loginError();
            AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
            androidApplication.setUserInfo(mContext, null);
        } else {
            Toasty.warning(mContext, errorMsg).show();
        }
    }
}
