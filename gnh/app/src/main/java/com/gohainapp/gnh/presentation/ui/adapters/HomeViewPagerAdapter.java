package com.gohainapp.gnh.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.gohainapp.gnh.R;
import com.gohainapp.gnh.domain.models.Banner;
import com.gohainapp.gnh.util.GlideHelper;

import org.jetbrains.annotations.NotNull;

public class HomeViewPagerAdapter extends PagerAdapter {

    Context mContext;
    Banner[] banners;

    public HomeViewPagerAdapter(Context mContext, Banner[] banners) {
        this.mContext = mContext;
        this.banners = banners;
    }

    @Override
    public int getCount() {
        return banners.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull @NotNull View view, @NonNull @NotNull Object object) {
         return view == object;
    }


    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.viewpager_item, container, false);
        ImageView imageView = layout.findViewById(R.id.item_imageView);
        GlideHelper.setImageViewCustomRoundedCorners(mContext, imageView, mContext.getResources().getString(R.string.base_url) + banners[position].image_path, 20);
        container.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }
}
