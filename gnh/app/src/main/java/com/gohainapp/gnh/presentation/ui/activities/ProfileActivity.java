package com.gohainapp.gnh.presentation.ui.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.gohainapp.gnh.R;
import com.gohainapp.gnh.domain.executors.impl.ThreadExecutor;
import com.gohainapp.gnh.domain.models.UserInfoWrapper;
import com.gohainapp.gnh.presentation.presenters.RegistrationActivityPresenter;
import com.gohainapp.gnh.presentation.presenters.impl.RegistrationActivityPresenterImpl;
import com.gohainapp.gnh.threading.MainThreadImpl;
import com.gohainapp.gnh.util.GlideHelper;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

import static com.gohainapp.gnh.util.Helper.calculateFileSize;
import static com.gohainapp.gnh.util.Helper.getRealPathFromURI;
import static com.gohainapp.gnh.util.Helper.saveImage;

public class ProfileActivity extends AppCompatActivity implements RegistrationActivityPresenter.View {

    String[] appPremisions = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
    };
    private static final int PERMISSIONS_REQUEST_CODE = 1240;
    private static final int REQUEST_PIC = 1000;
    String filePath = null;

    @BindView(R.id.text_view_create_profile)
    TextView textViewCreateProfile;
    @BindView(R.id.text_view_update_profile)
    TextView textViewUpdateProfile;
    @BindView(R.id.user_image)
    ImageView imageViewUserImage;
    @BindView(R.id.btn_create_profile)
    Button btn_create_profile;
    @BindView(R.id.btn_update_profile)
    Button btn_update_profile;
    @BindView(R.id.edit_text_first_name_registration)
    EditText editTextFirstName;
    @BindView(R.id.edit_text_last_name_registration)
    EditText editTextLastName;
    @BindView(R.id.edit_text_password_registration)
    EditText editTextPasswordRegistration;
    @BindView(R.id.edit_text_repeat_password_registration)
    EditText editTextRepeatPasswordRegistration;
    @BindView(R.id.registration_scroll_view)
    ScrollView registrationScrollView;
    @BindView(R.id.edit_text_phone_registration)
    EditText editTextPhone;
    @BindView(R.id.edit_text_address_registration)
    EditText editTextAddress;
    @BindView(R.id.edit_text_pin_registration)
    EditText editTextPin;
    @BindView(R.id.edit_text_email_registration)
    EditText editTextEmail;

    /////Layouts
    @BindView(R.id.text_input_first_name_registration_layout)
    TextInputLayout textInputFirstNameRegistrationLayout;
    @BindView(R.id.text_input_last_name_registration_layout)
    TextInputLayout textInputLastNameRegistrationLayout;
    @BindView(R.id.text_input_phone_registration_layout)
    TextInputLayout textInputPhoneRegistrationLayout;
    @BindView(R.id.text_input_email_registration_layout)
    TextInputLayout textInputEmailRegistrationLayout;
    @BindView(R.id.text_input_password_registration_layout)
    TextInputLayout textInputPasswordRegistrationLayout;
    @BindView(R.id.text_input_repeat_password_registration_layout)
    TextInputLayout textInputRepeatPasswordRegistrationLayout;
    @BindView(R.id.relative_layout_profile_pic)
    RelativeLayout relativeLayoutProfilePic;
    String userId;
    //////Layouts

    ProgressDialog progressDialog;
    RegistrationActivityPresenterImpl mPresenter;
    String mobile;

    int flag = 0 ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
        initialisePresenter();
        setProgressDialog();
        flag = getIntent().getIntExtra("flag" , 0);
        mobile = getIntent().getStringExtra("mobile");
        editTextPhone.setText(mobile);
        if(flag == 0) {
            mPresenter.fetchUserDetails();
            textViewCreateProfile.setVisibility(View.GONE);
            textViewUpdateProfile.setVisibility(View.VISIBLE);
            textInputPasswordRegistrationLayout.setVisibility(View.GONE);
            textInputRepeatPasswordRegistrationLayout.setVisibility(View.GONE);
            btn_create_profile.setVisibility(View.GONE);
            btn_update_profile.setVisibility(View.VISIBLE);
            relativeLayoutProfilePic.setVisibility(View.VISIBLE);
        }
    }

    private void setProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Its loading....");
        progressDialog.setTitle("Please Wait");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @OnClick(R.id.btn_create_profile) void onCreateProfileClicked () {
        textInputFirstNameRegistrationLayout.setError("");
        textInputLastNameRegistrationLayout.setError("");
        textInputEmailRegistrationLayout.setError("");
        textInputPasswordRegistrationLayout.setError("");
        textInputRepeatPasswordRegistrationLayout.setError("");

        if (editTextFirstName.getText().toString().trim().isEmpty()) {
            registrationScrollView.smoothScrollTo(0,textInputFirstNameRegistrationLayout.getTop());
            textInputFirstNameRegistrationLayout.setError("Please insert your first name");
        } else if (editTextLastName.getText().toString().trim().isEmpty()) {
            registrationScrollView.smoothScrollTo(0,textInputLastNameRegistrationLayout.getTop());
            textInputLastNameRegistrationLayout.setError("Please insert your Last name");
        } else if (editTextEmail.getText().toString().trim().isEmpty()) {
            textInputEmailRegistrationLayout.setError("Please insert your Email");
        } else if (editTextPasswordRegistration.getText().toString().trim().isEmpty()) {
            textInputPasswordRegistrationLayout.setError("Please insert your password");
        } else if(editTextPasswordRegistration.getText().toString().length() < 8) {
            textInputPasswordRegistrationLayout.setError("password must be at least 8 characters");
        } else if (editTextRepeatPasswordRegistration.getText().toString().trim().isEmpty()) {
            textInputRepeatPasswordRegistrationLayout.setError("Please insert your password");
        } else if (!editTextRepeatPasswordRegistration.getText().toString().trim().equals(editTextPasswordRegistration.getText().toString().trim())) {
            textInputRepeatPasswordRegistrationLayout.setError("Password and confirm password does not match");
        }
        else
        {
            mPresenter.registration(editTextFirstName.getText().toString(), editTextLastName.getText().toString(), editTextPhone.getText().toString(), editTextPasswordRegistration.getText().toString(), editTextRepeatPasswordRegistration.getText().toString(), editTextPin.getText().toString(), editTextAddress.getText().toString(), editTextEmail.getText().toString());
        }
    }

    private void initialisePresenter() {
        mPresenter = new RegistrationActivityPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @OnClick(R.id.btn_update_profile) void onUpdateProfileClicked () {
        textInputFirstNameRegistrationLayout.setError("");
        textInputLastNameRegistrationLayout.setError("");
        textInputEmailRegistrationLayout.setError("");
        textInputPhoneRegistrationLayout.setError("");

        if (editTextFirstName.getText().toString().trim().isEmpty()) {
            registrationScrollView.smoothScrollTo(0,textInputFirstNameRegistrationLayout.getTop());
            textInputFirstNameRegistrationLayout.setError("Please insert your first name");
        } else if (editTextLastName.getText().toString().trim().isEmpty()) {
            registrationScrollView.smoothScrollTo(0,textInputLastNameRegistrationLayout.getTop());
            textInputLastNameRegistrationLayout.setError("Please insert your Last name");
        } else {
            mPresenter.update(userId, editTextFirstName.getText().toString(), editTextLastName.getText().toString(), editTextPin.getText().toString(), editTextAddress.getText().toString(), editTextEmail.getText().toString(), filePath);
        }
    }

    @Override
    public void onRegistrationSuccess() {
        Intent intent = new Intent(this, RegistrationSuccessfullActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onUpdateSuccess() {
        Toasty.success(this,"Your profile has bee updated Successfully.").show();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void loadUserDetails(UserInfoWrapper userInfoWrapper) {
        editTextFirstName.setText(userInfoWrapper.firstname);
        editTextLastName.setText(userInfoWrapper.lastname);
        editTextEmail.setText(userInfoWrapper.email);
        editTextAddress.setText(userInfoWrapper.address);
        editTextPhone.setText(userInfoWrapper.phone);
        editTextPin.setText(userInfoWrapper.pinCode);
        userId = userInfoWrapper.id;
        if(userInfoWrapper.profile_pic!=null) {
            GlideHelper.setImageViewCustomRoundedCorners(this, imageViewUserImage ,getResources().getString(R.string.base_url) + userInfoWrapper.profile_pic,150);
        }
    }

    private boolean checkAndRequestPermissions(){
        List<String> listPermissionsNeeded = new ArrayList<>();
        for(String perm: appPremisions){
            if(ContextCompat.checkSelfPermission(this, perm) != PackageManager.PERMISSION_GRANTED){
                listPermissionsNeeded.add(perm);
            }
        }
        if(!listPermissionsNeeded.isEmpty()){
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), PERMISSIONS_REQUEST_CODE);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSIONS_REQUEST_CODE) {
            HashMap<String, Integer> permissionResults = new HashMap<>();
            int deniedCount = 0;
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    permissionResults.put(permissions[i], grantResults[i]);
                    deniedCount++;
                }
            }

            if (deniedCount == 0) {
                loadImageChooser();
            } else {
                for (Map.Entry<String, Integer> entry : permissionResults.entrySet()) {
                    String permName = entry.getKey();
                    int permResult = entry.getValue();
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, permName)) {
                        this.showAlertDialog("", "This app needs read and write storage permissions",
                                "Yes, Grant permissions",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        checkAndRequestPermissions();
                                    }
                                },
                                "Cancel",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        Toast.makeText(getApplicationContext(), "Permissions are required to download file", Toast.LENGTH_SHORT).show();
                                    }
                                },
                                false);
                    } else {
                        this.showAlertDialog("", "You have denied some permissions. Allow all permissions to download file at [Setting] > [Permissions]",
                                "Go to settings",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                                Uri.fromParts("package", getPackageName(), null));
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                    }
                                },
                                "Cancel",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        Toast.makeText(getApplicationContext(), "Permissions are required to download file", Toast.LENGTH_SHORT).show();
                                    }
                                },
                                false);
                        break;
                    }
                }
            }

        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Result code is RESULT_OK only if the user selects an Image
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_PIC) {
            if (data.getData() != null) {
                filePath = getRealPathFromURI(data.getData(), this);
                if (calculateFileSize(filePath) > 10) {
                    Toast.makeText(this, "Image should be less than 5 mb", Toast.LENGTH_SHORT).show();
                } else  {
                    GlideHelper.setImageViewCustomRoundedCornersWithUri(this, imageViewUserImage, data.getData(),150);
                }
            } else {
                Bitmap photo = (Bitmap) data.getExtras().get("data");
                filePath = saveImage(photo);
                if (calculateFileSize(filePath) > 10) {
                    Toast.makeText(this, "Image should be less than 5 mb", Toast.LENGTH_SHORT).show();
                } else {
                    GlideHelper.setImageViewCustomRoundedCornersWithBitmap(this, imageViewUserImage, photo,150);
                }

            }
        }
    }

    private void loadImageChooser() {
        Intent galleryintent = new Intent(Intent.ACTION_PICK);
        galleryintent.setType("image/*");

        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

        Intent chooser = new Intent(Intent.ACTION_CHOOSER);
        chooser.putExtra(Intent.EXTRA_INTENT, galleryintent);
        chooser.putExtra(Intent.EXTRA_TITLE, "Select from:");

        Intent[] intentArray = { cameraIntent };
        chooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray);
        startActivityForResult(chooser, REQUEST_PIC);
    }

    public AlertDialog showAlertDialog(
            String title, String msg, String positiveLabel,
            DialogInterface.OnClickListener positiveOnClick,
            String negativeLabel, DialogInterface.OnClickListener negativeOnClick,
            boolean isCancelable
    ){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setCancelable(isCancelable);
        builder.setMessage(msg);
        builder.setPositiveButton(positiveLabel, positiveOnClick);
        builder.setNegativeButton(negativeLabel, negativeOnClick);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        return alertDialog;
    }

    @OnClick(R.id.relative_layout_profile_pic) void onProfileImageClick () {
        if (checkAndRequestPermissions()) {
            loadImageChooser();
        }
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void loginError() {
        Toast.makeText(this, "Your session has expired !!", Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @OnClick(R.id.btn_back) void onBackClicked()
    {
        finish();
    }
}