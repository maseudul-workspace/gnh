package com.gohainapp.gnh.domain.interactors.impl;

import com.gohainapp.gnh.domain.executors.Executor;
import com.gohainapp.gnh.domain.executors.MainThread;
import com.gohainapp.gnh.domain.interactors.UpdateUserProfileInteractor;
import com.gohainapp.gnh.domain.interactors.base.AbstractInteractor;
import com.gohainapp.gnh.domain.models.UserInfoWrapper;
import com.gohainapp.gnh.repository.AppRepositoryImpl;

public class UpdateUserProfileInteractorImpl extends AbstractInteractor implements UpdateUserProfileInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String authorization;
    String userId;
    String email;
    String firstname;
    String lastname;
    String address;
    String pin_code;
    String filePath;

    public UpdateUserProfileInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String authorization, String userId, String email, String firstname, String lastname, String address, String pin_code, String filePath) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.authorization = authorization;
        this.userId = userId;
        this.email = email;
        this.firstname = firstname;
        this.lastname = lastname;
        this.address = address;
        this.pin_code = pin_code;
        this.filePath = filePath;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onUpdateProfileFail(errorMsg);
            }
        });
    }

    private void postMessage(UserInfoWrapper userInfoWrapper){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onUpdateProfileSuccess(userInfoWrapper);
            }
        });
    }

    @Override
    public void run() {
        final UserInfoWrapper userInfoWrapper = mRepository.profileUpdate(authorization, userId, firstname,lastname, email, address, pin_code, filePath);
        if (userInfoWrapper  == null) {
            notifyError("Please Check Your Internet Connection");
        } else if(userInfoWrapper .status == 400) {
            notifyError(userInfoWrapper .messages.medicine_desc);
        } else if(userInfoWrapper .error != null){
            notifyError("invalid_token");
        } else {
            postMessage(userInfoWrapper);
        }
    }
}
