package com.gohainapp.gnh.presentation.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.gohainapp.gnh.R;
import com.gohainapp.gnh.domain.models.RequestLabMedicineHistoryData;
import com.gohainapp.gnh.presentation.ui.activities.PhotoViewActivity;
import com.gohainapp.gnh.util.GlideHelper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PreviousLabRequestItemsRecyclerViewAdapter extends RecyclerView.Adapter<PreviousLabRequestItemsRecyclerViewAdapter.ViewHolder> {

    Context mContext;
    RequestLabMedicineHistoryData[] requestLabMedicineHistories;
    Callback mCallback;

    public interface Callback {
        void onPaymentClicked(int id, int type);
    }

    public PreviousLabRequestItemsRecyclerViewAdapter(Context mContext, RequestLabMedicineHistoryData[] requestLabMedicineHistories, Callback mCallback) {
        this.mContext = mContext;
        this.requestLabMedicineHistories = requestLabMedicineHistories;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_previous_lab_history_items, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewName.setText(requestLabMedicineHistories[position].medicine_desc);
        if(!requestLabMedicineHistories[position].delivery_boy_name.isEmpty()) {
            holder.txtViewDeliveryBoyName.setVisibility(View.VISIBLE);
            holder.txtViewDeliveryBoyName.setText("Delivered By: " + requestLabMedicineHistories[position].delivery_boy_name);
        }
        if(!requestLabMedicineHistories[position].delivery_boy_phone.isEmpty()) {
            holder.phoneLayout.setVisibility(View.VISIBLE);
            holder.txtViewDeliveryBoyPhone.setText(requestLabMedicineHistories[position].delivery_boy_phone);
        }
        if(!requestLabMedicineHistories[position].delivery_date.equals("0000-00-00 00:00:00")) {
            holder.txtViewTime.setVisibility(View.VISIBLE);
            holder.txtViewDate.setVisibility(View.VISIBLE);
            holder.txtViewDate.setText(convertToDate(requestLabMedicineHistories[position].delivery_date));
            holder.txtViewTime.setText(convertToTime(requestLabMedicineHistories[position].delivery_date));
        }
        GlideHelper.setImageView(mContext, holder.imageViewPrescription, mContext.getResources().getString(R.string.base_url) + requestLabMedicineHistories[position].prescription);
        holder.imageViewPrescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToPhotoViewActivity(mContext.getResources().getString(R.string.base_url) + requestLabMedicineHistories[position].prescription);
            }
        });
        holder.btnPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onPaymentClicked(Integer.parseInt(requestLabMedicineHistories[position].id), 3);
            }
        });

           if(!requestLabMedicineHistories[position].amount.equals("0.00")) {
               holder.txtViewRate.setVisibility(View.VISIBLE);
               holder.txtViewRate.setText("Amount: ₹ "+requestLabMedicineHistories[position].amount);
           }

        switch (requestLabMedicineHistories[position].status)
        {
            case "1" :
                holder.viewBlue.setVisibility(View.VISIBLE);
                holder.viewGreen.setVisibility(View.GONE);
                holder.viewRed.setVisibility(View.GONE);
                holder.viewYellow.setVisibility(View.GONE);
                holder.btnPay.setVisibility(View.GONE);
                holder.txtViewOrderStatus.setText("Requested");
                break;

            case "2" :
                holder.viewBlue.setVisibility(View.GONE);
                holder.viewGreen.setVisibility(View.VISIBLE);
                holder.viewRed.setVisibility(View.GONE);
                holder.viewYellow.setVisibility(View.GONE);
                holder.txtViewOrderStatus.setText("Accepted");
                break;

            case "4" :
                holder.viewBlue.setVisibility(View.GONE);
                holder.viewGreen.setVisibility(View.GONE);
                holder.viewRed.setVisibility(View.GONE);
                holder.viewYellow.setVisibility(View.VISIBLE);
                holder.txtViewOrderStatus.setText("Payment Pending");
                break;

            case "5" :
                holder.viewBlue.setVisibility(View.GONE);
                holder.viewGreen.setVisibility(View.VISIBLE);
                holder.viewRed.setVisibility(View.GONE);
                holder.viewYellow.setVisibility(View.GONE);
                holder.btnPay.setVisibility(View.GONE);
                holder.txtViewOrderStatus.setText("Paid");
                break;

            case "6" :
                holder.viewBlue.setVisibility(View.GONE);
                holder.viewGreen.setVisibility(View.VISIBLE);
                holder.viewRed.setVisibility(View.GONE);
                holder.viewYellow.setVisibility(View.GONE);
                holder.btnPay.setVisibility(View.GONE);
                holder.txtViewOrderStatus.setText("Prescription Generated");
                break;

            case "7" :
                holder.viewBlue.setVisibility(View.GONE);
                holder.viewGreen.setVisibility(View.GONE);
                holder.viewRed.setVisibility(View.VISIBLE);
                holder.viewYellow.setVisibility(View.GONE);
                holder.btnPay.setVisibility(View.GONE);
                holder.txtViewOrderStatus.setText("Payment Failed");
                break;

            case "8" :
                holder.viewBlue.setVisibility(View.GONE);
                holder.viewGreen.setVisibility(View.GONE);
                holder.viewRed.setVisibility(View.VISIBLE);
                holder.viewYellow.setVisibility(View.GONE);
                holder.btnPay.setVisibility(View.GONE);
                holder.txtViewOrderStatus.setText("Rejected");
                break;

            case "9" :
                holder.viewBlue.setVisibility(View.GONE);
                holder.viewGreen.setVisibility(View.VISIBLE);
                holder.viewRed.setVisibility(View.GONE);
                holder.viewYellow.setVisibility(View.GONE);
                holder.btnPay.setVisibility(View.GONE);
                holder.txtViewOrderStatus.setText("Amount Refunded");
                break;

            case "10" :
                holder.viewBlue.setVisibility(View.GONE);
                holder.viewGreen.setVisibility(View.GONE);
                holder.viewRed.setVisibility(View.VISIBLE);
                holder.viewYellow.setVisibility(View.GONE);
                holder.btnPay.setVisibility(View.GONE);
                holder.txtViewOrderStatus.setText("Cancelled");
                break;

            case "11" :
                holder.viewBlue.setVisibility(View.GONE);
                holder.viewGreen.setVisibility(View.GONE);
                holder.viewRed.setVisibility(View.GONE);
                holder.viewYellow.setVisibility(View.VISIBLE);
                holder.btnPay.setVisibility(View.GONE);
                holder.txtViewOrderStatus.setText("Out for Delivery");
                break;

            case "12" :
                holder.viewBlue.setVisibility(View.GONE);
                holder.viewGreen.setVisibility(View.VISIBLE);
                holder.viewRed.setVisibility(View.GONE);
                holder.viewYellow.setVisibility(View.GONE);
                holder.btnPay.setVisibility(View.GONE);
                holder.txtViewOrderStatus.setText("Delivered");
                break;
        }
    }

    @Override
    public int getItemCount() {
        return requestLabMedicineHistories.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_Name)
        TextView txtViewName;
        @BindView(R.id.txt_view_delivery_boy_name)
        TextView txtViewDeliveryBoyName;
        @BindView(R.id.txt_view_delivery_boy_phone)
        TextView txtViewDeliveryBoyPhone;
        @BindView(R.id.txt_view_Date)
        TextView txtViewDate;
        @BindView(R.id.txt_view_time)
        TextView txtViewTime;
        @BindView(R.id.txt_view_rate)
        TextView txtViewRate;
        @BindView(R.id.image_view_prescription)
        ImageView imageViewPrescription;
        @BindView(R.id.text_view_order_status)
        TextView txtViewOrderStatus;
        @BindView(R.id.btn_pay)
        Button btnPay;
        @BindView(R.id.red)
        View viewRed;
        @BindView(R.id.blue)
        View viewBlue;
        @BindView(R.id.green)
        View viewGreen;
        @BindView(R.id.yellow)
        View viewYellow;
        @BindView(R.id.main_layout_class)
        CardView mainLayoutClass;
        @BindView(R.id.phone_layout)
        View phoneLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private String convertToDate(String date)
    {
        SimpleDateFormat spf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date newDate= null;
        try {
            newDate = spf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        spf= new SimpleDateFormat("EEEE, MMM d, yyyy");
        date = spf.format(newDate);
        return date;
    }

    private String convertToTime(String date)
    {
        SimpleDateFormat spf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date newDate= null;
        try {
            newDate = spf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        spf= new SimpleDateFormat("h:mm a");
        date = spf.format(newDate);
        return date;
    }

    private void goToPhotoViewActivity (String photoUrl) {
        Intent intent = new Intent(mContext, PhotoViewActivity.class);
        intent.putExtra("photoUrl", photoUrl);
        mContext.startActivity(intent);
    }

}
