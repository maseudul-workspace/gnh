package com.gohainapp.gnh.domain.interactors;

public interface GenerateOtpInteractor {
    interface Callback {
        void onGenerateOtpSuccess();
        void onGenerateOtpFailed(String errorMsg);
    }
}
