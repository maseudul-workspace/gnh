package com.gohainapp.gnh.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VideoConsultationRequestResponse {

    @SerializedName("status")
    @Expose
    public int status;

    @SerializedName("messages")
    @Expose
    public Messages messages;

    @SerializedName("error")
    @Expose
    public String error;

    @SerializedName("doctor_id")
    @Expose
    public String doctor_id;

    @SerializedName("appointment_time")
    @Expose
    public String appointment_time;

    @SerializedName("user_id")
    @Expose
    public String user_id;

    @SerializedName("amount")
    @Expose
    public String amount;

    @SerializedName("id")
    @Expose
    public int id;

}
