package com.gohainapp.gnh.domain.interactors;

import com.gohainapp.gnh.domain.models.Banner;

public interface FetchBannerInteractor {
    interface Callback {
        void onBannerFetchSuccess(Banner[] banners);
        void onBannerFetchFail(String errorMsg);
    }
}
