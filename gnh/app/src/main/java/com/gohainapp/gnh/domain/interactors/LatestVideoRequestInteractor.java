package com.gohainapp.gnh.domain.interactors;

import com.gohainapp.gnh.domain.models.RequestVideoHistoryData;

public interface LatestVideoRequestInteractor {
    interface Callback {
        void onLatestVideoRequestFetchSuccess(RequestVideoHistoryData[] requestVideoHistories);
        void onLatestVideoRequestFetchFail(String errorMsg);
    }
}
