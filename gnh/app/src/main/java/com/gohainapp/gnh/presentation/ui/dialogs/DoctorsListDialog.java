package com.gohainapp.gnh.presentation.ui.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.view.View;

import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gohainapp.gnh.R;
import com.gohainapp.gnh.domain.models.DoctorsList;
import com.gohainapp.gnh.presentation.ui.adapters.DoctorsListAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DoctorsListDialog implements DoctorsListAdapter.Callback {

    @Override
    public void onDoctorsSelected(DoctorsList doctorsList) {
        mCallback.onDoctorSelect(doctorsList);
    }

    public interface Callback {
        void onDoctorSelect(DoctorsList doctorsList);
    }

    Context mContext;
    View dialogContainer;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    Activity mActivity;
    Callback mCallback;
    @BindView(R.id.recycler_view_doctors)
    RecyclerView recyclerViewDoctors;


    public DoctorsListDialog(Context mContext, Activity mActivity, Callback mCallback) {
        this.mContext = mContext;
        this.mActivity = mActivity;
        this.mCallback = mCallback;
    }

    public void setUpDialog() {
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.layout_doctors_list_dialog, null);
        builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Material_Light_Dialog_NoActionBar_MinWidth);
        builder.setView(dialogContainer);
        dialog = builder.create();
        ButterKnife.bind(this, dialogContainer);
    }

    public void setDoctors(DoctorsList[] doctors) {
        DoctorsListAdapter adapter = new DoctorsListAdapter(mContext, doctors, this);
        recyclerViewDoctors.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerViewDoctors.setAdapter(adapter);
        recyclerViewDoctors.addItemDecoration(new DividerItemDecoration(recyclerViewDoctors.getContext(), DividerItemDecoration.VERTICAL));
    }

    public void showDialog() {
        dialog.show();
    }

    public void hideDialog() {
        dialog.dismiss();
    }

}
