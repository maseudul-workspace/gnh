package com.gohainapp.gnh.domain.interactors.impl;

import com.gohainapp.gnh.domain.executors.Executor;
import com.gohainapp.gnh.domain.executors.MainThread;
import com.gohainapp.gnh.domain.interactors.LabMedicineRequestInteractor;
import com.gohainapp.gnh.domain.interactors.base.AbstractInteractor;
import com.gohainapp.gnh.domain.models.LabMedicineRequestResponse;
import com.gohainapp.gnh.repository.AppRepositoryImpl;

public class LabMedicineRequestInteractorImpl extends AbstractInteractor implements LabMedicineRequestInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String authorization;
    int type;
    String medicine_desc;
    String filePath;

    public LabMedicineRequestInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String authorization, int type, String medicine_desc, String filePath) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.authorization = authorization;
        this.type = type;
        this.medicine_desc = medicine_desc;
        this.filePath = filePath;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onLabMedicineRequestFail(errorMsg);
            }
        });
    }

    private void postMessage(LabMedicineRequestResponse labMedicineRequestResponse){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onLabMedicineRequestSuccess(labMedicineRequestResponse);
            }
        });
    }

    @Override
    public void run() {
        final LabMedicineRequestResponse labMedicineRequestResponse = mRepository.requestLabMedicineTest(authorization, type, medicine_desc, filePath);
        if (labMedicineRequestResponse == null) {
            notifyError("Please Check Your Internet Connection");
        } else if(labMedicineRequestResponse.status == 400) {
            notifyError(labMedicineRequestResponse.messages.medicine_desc);
        } else if(labMedicineRequestResponse.error != null){
            notifyError("invalid_token");
        } else {
            postMessage(labMedicineRequestResponse);
        }
    }
}
