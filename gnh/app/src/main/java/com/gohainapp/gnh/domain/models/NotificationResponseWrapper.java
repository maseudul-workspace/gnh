package com.gohainapp.gnh.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NotificationResponseWrapper {

    @SerializedName("data")
    @Expose
    public NotificationResponseData[] notificationResponseData;

    @SerializedName("total_notification")
    @Expose
    public TotalNotificationData totalNotificationData;

    @SerializedName("error")
    @Expose
    public String error;

    @SerializedName("error_description")
    @Expose
    public String error_description;
}
