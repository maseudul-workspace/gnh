package com.gohainapp.gnh.domain.interactors.impl;

import com.gohainapp.gnh.domain.executors.Executor;
import com.gohainapp.gnh.domain.executors.MainThread;
import com.gohainapp.gnh.domain.interactors.FetchUserProfileInteractor;
import com.gohainapp.gnh.domain.interactors.base.AbstractInteractor;
import com.gohainapp.gnh.domain.models.UserInfoWrapper;
import com.gohainapp.gnh.repository.AppRepositoryImpl;

public class FetchUserProfileInteractorImpl extends AbstractInteractor implements FetchUserProfileInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String authorization;

    public FetchUserProfileInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String authorization) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.authorization = authorization;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onUserProfileFetchFail(errorMsg);
            }
        });
    }

    private void postMessage(UserInfoWrapper userInfoWrapper){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onUserProfileFetchSuccess(userInfoWrapper);
            }
        });
    }

    @Override
    public void run() {
        final UserInfoWrapper userInfoWrapper = mRepository.fetchUserProfile(authorization);
        if (userInfoWrapper == null) {
            notifyError("Please Check Your Internet Connection");
        } else if(userInfoWrapper.error != null){
            notifyError("invalid_token");
        } else {
            postMessage(userInfoWrapper);
        }
    }
}
