package com.gohainapp.gnh.presentation.presenters;

import com.gohainapp.gnh.domain.models.DoctorsList;

public interface VideoConsultationPresenter {
    void fetchDoctors();
    void sendBookingRequest(int doctor_id, String appointment_date);
    interface View {
        void loadDoctorsList(DoctorsList[] doctors);
        void onBookingRequestSuccess();
        void showLoader();
        void hideLoader();
        void loginError();
    }
}
