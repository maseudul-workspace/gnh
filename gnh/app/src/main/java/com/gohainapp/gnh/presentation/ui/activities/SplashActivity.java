package com.gohainapp.gnh.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

import butterknife.ButterKnife;

import com.gohainapp.gnh.AndroidApplication;
import com.gohainapp.gnh.R;
import com.gohainapp.gnh.domain.models.LoginResponse;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                if (checkLogin()) {
                    goToMainActivity();
                } else {
                    goToLoginActivity();
                }
            }
        }, 5000);
    }


    private boolean checkLogin() {
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        LoginResponse userInfo = androidApplication.getUserInfo(this);
        if (userInfo == null) {
            return false;
        } else {
            return true;
        }
    }

    private void goToMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void goToLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
}