package com.gohainapp.gnh.presentation.presenters;

import com.gohainapp.gnh.domain.models.RequestLabMedicineHistoryData;

public interface LabRequestHistoryPresenter {
    void fetchLatestRequest(int type);
    void fetchPreviousRequest(int type);
    interface View {
        void loadLatestRequest(RequestLabMedicineHistoryData[] requestLabMedicineHistories);
        void loadPreviousRequest(RequestLabMedicineHistoryData[] requestLabMedicineHistories);
        void showLoader();
        void hideLoader();
        void loginError();
    }
}
