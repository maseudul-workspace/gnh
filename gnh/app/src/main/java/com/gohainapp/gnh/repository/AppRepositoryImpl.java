package com.gohainapp.gnh.repository;

import android.util.Log;

import com.gohainapp.gnh.domain.models.BannerWrapper;
import com.gohainapp.gnh.domain.models.CommonResponse;
import com.gohainapp.gnh.domain.models.DoctorsListResponse;
import com.gohainapp.gnh.domain.models.LabMedicineRequestResponse;
import com.gohainapp.gnh.domain.models.LogOutResponse;
import com.gohainapp.gnh.domain.models.NotificationResponseWrapper;
import com.gohainapp.gnh.domain.models.OrderByRequestResponse;
import com.gohainapp.gnh.domain.models.PaymentVerificationResponse;
import com.gohainapp.gnh.domain.models.RequestLabMedicineHistoryResponse;
import com.gohainapp.gnh.domain.models.RequestVideoHistoryResponse;
import com.gohainapp.gnh.domain.models.TotalNotificationData;
import com.gohainapp.gnh.domain.models.UserInfoWrapper;
import com.gohainapp.gnh.domain.models.VideoConsultationRequestResponse;
import com.google.gson.Gson;


import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class AppRepositoryImpl {

    AppRepository mRepository;

    public AppRepositoryImpl() {
        mRepository = ApiClient.createService(AppRepository.class);
    }

    public CommonResponse generateOtp(String mobile) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> send = mRepository.generateOtp(mobile);
            Response<ResponseBody> response = send.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse generateOtpReset(String mobile) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> send = mRepository.generateOtpReset(mobile);
            Response<ResponseBody> response = send.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }


    public CommonResponse checkOtp(String mobile, String otp) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> verify = mRepository.checkOtp(mobile, otp);
            Response<ResponseBody> response = verify.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                   commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public UserInfoWrapper registration(String firstname, String lastname, String email, String phone, String password, String password_confirm, String address, String pinCode) {
        UserInfoWrapper userInfoWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> verify = mRepository.registration(firstname, lastname, email, phone, password, password_confirm, address, pinCode);
            Response<ResponseBody> response = verify.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    userInfoWrapper = null;
                }else{
                    userInfoWrapper = gson.fromJson(responseBody, UserInfoWrapper.class);
                }
            } else {
                userInfoWrapper = null;
            }
        }catch (Exception e){
            userInfoWrapper = null;
        }
        return userInfoWrapper;
    }

    public LogOutResponse logOut(String token) {
        LogOutResponse logOutResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> verify = mRepository.logOut(token);
            Response<ResponseBody> response = verify.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    logOutResponse = null;
                }else{
                    logOutResponse = gson.fromJson(responseBody, LogOutResponse.class);
                }
            } else {
                logOutResponse = null;
            }
        }catch (Exception e){
            logOutResponse = null;
        }
        return logOutResponse;
    }

    public UserInfoWrapper changePassword( String phone, String password, String password_confirm) {
        UserInfoWrapper userInfoWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> verify = mRepository.changePassword(phone, password, password_confirm);
            Response<ResponseBody> response = verify.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    userInfoWrapper = null;
                }else{
                    userInfoWrapper = gson.fromJson(responseBody, UserInfoWrapper.class);
                }
            } else {
                userInfoWrapper = null;
            }
        }catch (Exception e){
            userInfoWrapper = null;
        }
        return userInfoWrapper;
    }

        public DoctorsListResponse fetchDoctorsList(String apiToken) {
        DoctorsListResponse doctors;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchDoctorList("Bearer " + apiToken);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    doctors = null;
                }else{
                    doctors = gson.fromJson(responseBody, DoctorsListResponse.class);
                }
            } else {
                doctors = null;
            }
        }catch (Exception e){
            doctors = null;
        }
        return doctors;
    }

    public RequestVideoHistoryResponse latestVideoRequest(String apiToken) {
        RequestVideoHistoryResponse requestVideoHistories;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.getLatestVideoRequest("Bearer " + apiToken);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    requestVideoHistories = null;
                }else{
                    requestVideoHistories = gson.fromJson(responseBody, RequestVideoHistoryResponse.class);
                }
            } else {
                requestVideoHistories = null;
            }
        }catch (Exception e){
            requestVideoHistories = null;
        }
        return requestVideoHistories;
    }

    public RequestVideoHistoryResponse previousVideoRequest(String apiToken) {
        RequestVideoHistoryResponse requestVideoHistories;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.getPreviousVideoRequest("Bearer " + apiToken);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    requestVideoHistories = null;
                }else{
                    requestVideoHistories = gson.fromJson(responseBody, RequestVideoHistoryResponse.class);
                }
            } else {
                requestVideoHistories = null;
            }
        }catch (Exception e){
            requestVideoHistories = null;
        }
        return requestVideoHistories;
    }


        public VideoConsultationRequestResponse requestVideoConsultation(String authorization, int doctor_id, String appointment_date)
            {

        VideoConsultationRequestResponse videoConsultationRequestResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> place = mRepository.requestVideoConsultation("Bearer " + authorization, appointment_date, doctor_id);
            Response<ResponseBody> response = place.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    videoConsultationRequestResponse = null;
                }else{
                    videoConsultationRequestResponse = gson.fromJson(responseBody, VideoConsultationRequestResponse.class);
                }
            } else {
                videoConsultationRequestResponse = null;
            }
        }catch (Exception e){
            videoConsultationRequestResponse = null;
        }
        return videoConsultationRequestResponse;
    }

    public OrderByRequestResponse getOrderByRequest(int id, int type) {
        OrderByRequestResponse orderByRequestResponse;
        String responseBody = "";
        Gson gson = new Gson();
        try {
            Call<ResponseBody> verify = mRepository.getOrderByRequest(id, type);
            Response<ResponseBody> response = verify.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (!responseBody.isEmpty()) {
                orderByRequestResponse = gson.fromJson(responseBody, OrderByRequestResponse.class);
            } else {
                orderByRequestResponse = null;
            }
        }catch (Exception e){
            orderByRequestResponse = null;
        }
        return orderByRequestResponse;
    }

    public PaymentVerificationResponse verifyPayment(String authorization,
                                                     String razorpayOrderId,
                                                     String razorpayPaymentId,
                                                     String razorpaySignature) {
        PaymentVerificationResponse paymentVerificationResponse;
        String responseBody = "";
        Gson gson = new Gson();
        try {
            Call<ResponseBody> verify = mRepository.verifyPayment("Bearer " + authorization, razorpayOrderId, razorpayPaymentId, razorpaySignature);
            Response<ResponseBody> response = verify.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (!responseBody.isEmpty()) {
                paymentVerificationResponse = gson.fromJson(responseBody, PaymentVerificationResponse.class);
            } else {
                paymentVerificationResponse = null;
            }
        }catch (Exception e){
            paymentVerificationResponse = null;
        }
        return paymentVerificationResponse;
    }

    public LabMedicineRequestResponse requestLabMedicineTest(String authorization, int type, String medicine_desc, String filePath)
    {
        LabMedicineRequestResponse labMedicineRequestResponse;

        RequestBody requestType = RequestBody.create(okhttp3.MultipartBody.FORM, Integer.toString(type));
        RequestBody description = RequestBody.create(okhttp3.MultipartBody.FORM, medicine_desc);

        File imageFile = new File(filePath);

        // create RequestBody instance from file
        RequestBody requestImageFile =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"),
                        imageFile
                );

        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part imageFileBody =
                MultipartBody.Part.createFormData("prescription", imageFile.getName(), requestImageFile);

        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> place = mRepository.requestLabMedicine("Bearer " + authorization, requestType, description, imageFileBody);
            Response<ResponseBody> response = place.execute();
            if(response.body() != null) {
                responseBody = response.body().string();
            } else if(response.errorBody() != null) {
                responseBody = response.errorBody().string();
            }
            Log.e("ApiError", responseBody);
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    labMedicineRequestResponse = null;
                }else{
                    labMedicineRequestResponse = gson.fromJson(responseBody, LabMedicineRequestResponse.class);
                }
            } else {
                labMedicineRequestResponse = null;
            }
        }catch (Exception e){
            Log.e("ApiError", e.toString());
            labMedicineRequestResponse = null;
        }
        return labMedicineRequestResponse;
    }

    public RequestLabMedicineHistoryResponse latestLabMedicineRequest(String apiToken, int type) {
        RequestLabMedicineHistoryResponse requestLabMedicineHistories;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.getLatestLabMedicineRequest("Bearer " + apiToken, type);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    requestLabMedicineHistories = null;
                }else{
                    requestLabMedicineHistories = gson.fromJson(responseBody, RequestLabMedicineHistoryResponse.class);
                }
            } else {
                requestLabMedicineHistories = null;
            }
        }catch (Exception e){
            requestLabMedicineHistories = null;
        }
        return requestLabMedicineHistories;
    }

    public RequestLabMedicineHistoryResponse previousLabMedicineRequest(String apiToken, int type) {
        RequestLabMedicineHistoryResponse requestLabMedicineHistories;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.getPreviousLabMedicineRequest("Bearer " + apiToken, type);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    requestLabMedicineHistories = null;
                }else{
                    requestLabMedicineHistories = gson.fromJson(responseBody, RequestLabMedicineHistoryResponse.class);
                }
            } else {
                requestLabMedicineHistories = null;
            }
        }catch (Exception e){
            requestLabMedicineHistories = null;
        }
        return requestLabMedicineHistories;
    }

    public UserInfoWrapper fetchUserProfile(String apiToken) {
        UserInfoWrapper userInfoWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> verify = mRepository.fetchUserProfile("Bearer " + apiToken);
            Response<ResponseBody> response = verify.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    userInfoWrapper = null;
                }else{
                    userInfoWrapper = gson.fromJson(responseBody, UserInfoWrapper.class);
                }
            } else {
                userInfoWrapper = null;
            }
        }catch (Exception e){
            userInfoWrapper = null;
        }
        return userInfoWrapper;
    }

    public BannerWrapper fetchBanner(String apiToken) {
        BannerWrapper banner;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> verify = mRepository.fetchBanner("Bearer " + apiToken);
            Response<ResponseBody> response = verify.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    banner = null;
                }else{
                    banner = gson.fromJson(responseBody, BannerWrapper.class);
                }
            } else {
                banner = null;
            }
        }catch (Exception e){
            banner = null;
        }
        return banner;
    }

    public UserInfoWrapper profileUpdate(String apiToken, String userId, String firstname, String lastname, String email, String address, String pinCode, String filePath) {
        UserInfoWrapper userInfoWrapper;

        RequestBody user_id = RequestBody.create(okhttp3.MultipartBody.FORM, userId);
        RequestBody user_mail = RequestBody.create(okhttp3.MultipartBody.FORM, email);
        RequestBody user_first_name = RequestBody.create(okhttp3.MultipartBody.FORM, firstname);
        RequestBody user_last_name = RequestBody.create(okhttp3.MultipartBody.FORM, lastname);
        RequestBody user_address = RequestBody.create(okhttp3.MultipartBody.FORM, address);
        RequestBody user_pincode = RequestBody.create(okhttp3.MultipartBody.FORM, pinCode);
        MultipartBody.Part imageFileBody = null;

        if (filePath != null) {
            File imageFile = new File(filePath);

            // create RequestBody instance from file
            RequestBody requestImageFile =
                    RequestBody.create(
                            MediaType.parse("multipart/form-data"),
                            imageFile
                    );

            // MultipartBody.Part is used to send also the actual file name
            imageFileBody =
                    MultipartBody.Part.createFormData("profile_pic", imageFile.getName(), requestImageFile);
        }

        String responseBody = "";
        Gson gson = new Gson();
        try {
            Call<ResponseBody> verify = mRepository.profileUpdate("Bearer " + apiToken, user_id, user_first_name, user_last_name, user_mail, user_address, user_pincode, imageFileBody);
            Response<ResponseBody> response = verify.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (!responseBody.isEmpty()) {
                userInfoWrapper = gson.fromJson(responseBody, UserInfoWrapper.class);
            } else {
                userInfoWrapper = null;
            }
        }catch (Exception e){
            userInfoWrapper = null;
        }
        return userInfoWrapper;
    }

    public NotificationResponseWrapper fetchNotifications(String apiToken, int offset, int limit) {
        NotificationResponseWrapper notificationResponseWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchNotifications("Bearer " + apiToken, offset, limit);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (!responseBody.isEmpty()) {
                notificationResponseWrapper = gson.fromJson(responseBody, NotificationResponseWrapper.class);
            } else {
                notificationResponseWrapper = null;
            }
        }catch (Exception e){
            notificationResponseWrapper = null;
        }
        return notificationResponseWrapper;
    }

    public TotalNotificationData getUnreadNotification(String apiToken) {
        TotalNotificationData totalNotificationData;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.getUnreadNotification("Bearer " + apiToken);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (!responseBody.isEmpty()) {
                totalNotificationData = gson.fromJson(responseBody, TotalNotificationData.class);
            } else {
                totalNotificationData = null;
            }
        }catch (Exception e){
            totalNotificationData = null;
        }
        return totalNotificationData;
    }

}
