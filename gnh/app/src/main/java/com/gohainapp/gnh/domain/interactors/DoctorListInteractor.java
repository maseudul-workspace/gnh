package com.gohainapp.gnh.domain.interactors;

import com.gohainapp.gnh.domain.models.DoctorsList;

public interface DoctorListInteractor {
    interface Callback {
        void onDoctorsListFetchSuccess(DoctorsList[] doctors);
        void onDoctorsListFetchFail(String errorMsg);
    }
}
