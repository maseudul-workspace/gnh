package com.gohainapp.gnh.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.gohainapp.gnh.R;
import com.gohainapp.gnh.domain.models.DoctorsList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DoctorsListAdapter extends RecyclerView.Adapter<DoctorsListAdapter.ViewHolder> {

    public interface Callback {
        void onDoctorsSelected(DoctorsList doctorsListResponse);
    }

    Context mContext;
    DoctorsList[] doctors;
    Callback mCallback;

    public DoctorsListAdapter(Context mContext, DoctorsList[] doctors, Callback callback) {
        this.mContext = mContext;
        this.doctors = doctors;
        this.mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_doctor_item_list, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewDoctorsName.setText(doctors[position].doctor_name);
        holder.txtViewDoctorsDesignation.setText("[ "+doctors[position].designation+" ]");
        holder.txtViewDoctorsPrice.setText("₹ "+doctors[position].rate);
        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onDoctorsSelected(doctors[holder.getAdapterPosition()]);
            }
        });
    }

    @Override
    public int getItemCount() {
        return doctors.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_doctor)
        TextView txtViewDoctorsName;
        @BindView(R.id.txt_view_doctor_designation)
        TextView txtViewDoctorsDesignation;
        @BindView(R.id.txt_view_doctors_price)
        TextView txtViewDoctorsPrice;
        @BindView(R.id.main_layout_class)
        View mainLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
