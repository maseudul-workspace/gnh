package com.gohainapp.gnh.domain.interactors;

import com.gohainapp.gnh.domain.models.LoginResponse;

public interface CheckLoginIntercator {
    interface Callback {
        void onLoginSuccess(LoginResponse loginResponse);
        void onLoginFail(String errorMessage);
    }
}
