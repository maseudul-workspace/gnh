package com.gohainapp.gnh.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PaymentVerificationResponse {

    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("razorpay_payment_id")
    @Expose
    public String razorpayPaymentId = null;

    @SerializedName("error")
    @Expose
    public String error = null;

}
