package com.gohainapp.gnh.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.app.ProgressDialog;
import android.os.Bundle;
import com.gohainapp.gnh.R;
import com.gohainapp.gnh.domain.executors.impl.ThreadExecutor;
import com.gohainapp.gnh.domain.models.OrderByRequestResponse;
import com.gohainapp.gnh.presentation.presenters.PaymentActivityPresenter;
import com.gohainapp.gnh.presentation.presenters.impl.PaymentActivityPresenterImpl;
import com.gohainapp.gnh.threading.MainThreadImpl;
import com.razorpay.Checkout;
import com.razorpay.PaymentData;
import com.razorpay.PaymentResultWithDataListener;

import org.json.JSONObject;

import java.util.Objects;

import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

public class PaymentActivity extends AppCompatActivity implements PaymentActivityPresenter.View, PaymentResultWithDataListener {

    PaymentActivityPresenterImpl mPresenter;
    ProgressDialog progressDialog;
    int id;
    int type;
    Checkout checkout = new Checkout();
    String amount;
    String name;
    PaymentData paymentData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        ButterKnife.bind(this);
        initialisePresenter();
        setProgressDialog();
        checkout.setImage(R.drawable.gnh_logo);
        id = getIntent().getIntExtra("id", 0);
        type = getIntent().getIntExtra("type",0);
        mPresenter.getOrderByRequest(id, type);
    }

    private void setProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Its loading....");
        progressDialog.setTitle("Please Wait");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    private void initialisePresenter() {
        mPresenter = new PaymentActivityPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @Override
    public void loadGetOrderResponse(OrderByRequestResponse orderByRequestResponse) {
        this.amount = orderByRequestResponse.amount;
        this.name = orderByRequestResponse.name;
        checkout.setKeyID(orderByRequestResponse.key);
        try {
            JSONObject options = new JSONObject();

            options.put("name", orderByRequestResponse.name);
            options.put("description", orderByRequestResponse.description);
            options.put("order_id", orderByRequestResponse.order_id);//from response of step 3.
            options.put("theme.color", orderByRequestResponse.theme.color);
            options.put("address", orderByRequestResponse.notes.address);
            options.put("currency", "INR");
            options.put("amount", orderByRequestResponse.amount);//pass amount in currency subunits
            options.put("prefill.email", orderByRequestResponse.prefill.email);
            options.put("prefill.contact",orderByRequestResponse.prefill.contact);
            options.put("image", orderByRequestResponse.image);
            JSONObject retryObj = new JSONObject();
            retryObj.put("enabled", true);
            retryObj.put("max_count", 4);
            options.put("retry", retryObj);

            checkout.open(this, options);

        } catch(Exception e) {
            Toasty.warning(this, Objects.requireNonNull(e.getMessage())).show();
        }
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void onPaymentSuccess(String s, PaymentData paymentData) {
        this.paymentData = paymentData;
        mPresenter.verifyPayment(paymentData.getOrderId(), paymentData.getPaymentId(), paymentData.getSignature());
    }

    @Override
    public void onPaymentError(int i, String s, PaymentData paymentData) {
        this.paymentData = paymentData;
        goToPaymentSuccessActivity(false);
    }

    @Override
    public void goToPaymentSuccessActivity(boolean isSuccess) {
        Intent intent = new Intent(this, PaymentSuccessActivity.class);
        intent.putExtra("signature", paymentData.getSignature());
        intent.putExtra("order_id", paymentData.getOrderId());
        intent.putExtra("payment_id", paymentData.getPaymentId());
        intent.putExtra("amount", this.amount);
        intent.putExtra("paid_by", this.name);
        intent.putExtra("is_success", isSuccess);
        startActivity(intent);
        finish();
    }

}