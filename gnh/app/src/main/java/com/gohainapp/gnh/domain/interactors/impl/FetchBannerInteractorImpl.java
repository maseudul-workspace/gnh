package com.gohainapp.gnh.domain.interactors.impl;

import com.gohainapp.gnh.domain.executors.Executor;
import com.gohainapp.gnh.domain.executors.MainThread;
import com.gohainapp.gnh.domain.interactors.FetchBannerInteractor;
import com.gohainapp.gnh.domain.interactors.base.AbstractInteractor;
import com.gohainapp.gnh.domain.models.Banner;
import com.gohainapp.gnh.domain.models.BannerWrapper;
import com.gohainapp.gnh.repository.AppRepositoryImpl;

public class FetchBannerInteractorImpl extends AbstractInteractor implements FetchBannerInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String authorization;

    public FetchBannerInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String authorization) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.authorization = authorization;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onBannerFetchFail(errorMsg);
            }
        });
    }

    private void postMessage(Banner[] banners){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onBannerFetchSuccess(banners);
            }
        });
    }

    @Override
    public void run() {
        final BannerWrapper bannerWrapper = mRepository.fetchBanner(authorization);
        if (bannerWrapper == null) {
            notifyError("Please Check Your Internet Connection");
        } else if (bannerWrapper.error != null) {
            notifyError("invalid_token");
        } else {
            postMessage(bannerWrapper.banners);
        }
    }
}
