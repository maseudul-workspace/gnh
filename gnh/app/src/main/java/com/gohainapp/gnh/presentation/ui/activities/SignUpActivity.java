package com.gohainapp.gnh.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.gohainapp.gnh.R;
import com.gohainapp.gnh.domain.executors.impl.ThreadExecutor;
import com.gohainapp.gnh.presentation.presenters.SignUpActivityPresenter;
import com.gohainapp.gnh.presentation.presenters.impl.SignUpActivityPresenterImpl;
import com.gohainapp.gnh.threading.MainThreadImpl;
import com.goodiebag.pinview.Pinview;
import com.google.android.material.textfield.TextInputLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

public class SignUpActivity extends AppCompatActivity implements SignUpActivityPresenter.View {

    @BindView(R.id.pin_view)
    Pinview pinView;
    @BindView(R.id.text_input_mobile_layout)
    TextInputLayout textInputLayoutMobile;
    @BindView(R.id.edit_text_mobile)
    EditText editTextMobile;
    @BindView(R.id.btn_get_otp)
    Button btnGetOtp;
    @BindView(R.id.btn_verify_otp)
    Button btnVerifyOtp;
    SignUpActivityPresenterImpl mPresenter;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);
        initialisePresenter();
        setProgressDialog();
    }

    private void setProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Its loading....");
        progressDialog.setTitle("Please Wait");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    private void initialisePresenter() {
        mPresenter = new SignUpActivityPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @OnClick(R.id.btn_login) void onLoginClick () {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finishAffinity();
    }

    @OnClick(R.id.btn_get_otp) void onGetOtpClick() {
        textInputLayoutMobile.setError("");
        if(editTextMobile.getText().toString().trim().isEmpty()) {
            textInputLayoutMobile.setError("Please Insert Phone Number");
        } else if(editTextMobile.getText().toString().trim().length() != 10)
        {
            textInputLayoutMobile.setError("Insert Valid Phone Number");
        } else
        {
            mPresenter.generateOtp(editTextMobile.getText().toString());
        }
    }

    @OnClick(R.id.btn_verify_otp) void onVerifyOtpClick() {
        if (pinView.getValue().length() < 4) {
            Toasty.warning(this, "OTP must be 4 digits").show();
        } else {
            mPresenter.checkOtp(editTextMobile.getText().toString(), pinView.getValue());
        }
    }

    @Override
    public void onOtpSendSuccess() {
        pinView.setVisibility(View.VISIBLE);
        textInputLayoutMobile.setVisibility(View.GONE);
        btnVerifyOtp.setVisibility(View.VISIBLE);
        btnGetOtp.setVisibility(View.GONE);
        Toast.makeText(this, "Otp send successfully please check your message and enter opt", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onOtpCheckSuccess() {
        Intent intent = new Intent(this, ProfileActivity.class);
        intent.putExtra("flag", 1);
        intent.putExtra("mobile", editTextMobile.getText().toString());
        startActivity(intent);
        finishAffinity();
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }
}