package com.gohainapp.gnh.domain.interactors.impl;

import com.gohainapp.gnh.domain.executors.Executor;
import com.gohainapp.gnh.domain.executors.MainThread;
import com.gohainapp.gnh.domain.interactors.ChangePasswordInteractor;
import com.gohainapp.gnh.domain.interactors.base.AbstractInteractor;
import com.gohainapp.gnh.domain.models.UserInfoWrapper;
import com.gohainapp.gnh.repository.AppRepositoryImpl;

public class ChangePasswordInteractorImpl extends AbstractInteractor implements ChangePasswordInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String phone;
    String password;
    String password_confirm;

    public ChangePasswordInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String phone, String password, String password_confirm) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.phone = phone;
        this.password = password;
        this.password_confirm = password_confirm;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onChangePasswordFail(errorMsg);
            }
        });
    }

    private void postMessage(UserInfoWrapper userInfo){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onChangePasswordSuccess(userInfo);
            }
        });
    }


    @Override
    public void run() {
        final UserInfoWrapper userInfoWrapper = mRepository.changePassword(phone, password, password_confirm);
        if (userInfoWrapper == null) {
            notifyError("Please Check Your Internet Connection");
        } else if (userInfoWrapper.status == 400) {
            notifyError(userInfoWrapper.messages.phone);
        } else {
            postMessage(userInfoWrapper);
        }
    }
}
