package com.gohainapp.gnh.domain.interactors.impl;

import com.gohainapp.gnh.domain.executors.Executor;
import com.gohainapp.gnh.domain.executors.MainThread;
import com.gohainapp.gnh.domain.interactors.CheckLoginIntercator;
import com.gohainapp.gnh.domain.interactors.base.AbstractInteractor;
import com.gohainapp.gnh.domain.models.LoginResponse;
import com.gohainapp.gnh.repository.LoginAppRepositoryImpl;

public class CheckLoginInteractorImpl extends AbstractInteractor implements CheckLoginIntercator {

    LoginAppRepositoryImpl mRepository;
    Callback mCallback;
    private String granType;
    private String username;
    private String password;

    public CheckLoginInteractorImpl(Executor threadExecutor, MainThread mainThread, LoginAppRepositoryImpl mRepository, Callback mCallback, String granType, String username, String password) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.granType = granType;
        this.username = username;
        this.password = password;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onLoginFail(errorMsg);
            }
        });
    }

    private void postMessage(LoginResponse loginResponse){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onLoginSuccess(loginResponse);
            }
        });
    }

    @Override
    public void run() {
        final LoginResponse loginResponse = mRepository.checkLogin(granType, username, password);
        if (loginResponse == null) {
            notifyError("Please Check Your Internet Connection");
        } else if (loginResponse.error != null) {
            notifyError(loginResponse.error_description);
        } else {
            postMessage(loginResponse);
        }
    }
}
