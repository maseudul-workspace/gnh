package com.gohainapp.gnh.presentation.presenters;

import com.gohainapp.gnh.domain.models.UserInfoWrapper;

public interface RegistrationActivityPresenter {
    void registration(String firstname, String lastname, String phone, String password, String password_confirm, String pinCode, String address, String email);
    void update(String userId, String firstname, String lastname, String pinCode, String address, String email, String filepath);
    void fetchUserDetails();
    interface View {
        void onRegistrationSuccess();
        void onUpdateSuccess();
        void loadUserDetails(UserInfoWrapper userInfoWrapper);
        void showLoader();
        void hideLoader();
        void loginError();
    }
}
