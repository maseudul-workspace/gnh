package com.gohainapp.gnh.domain.interactors;

import com.gohainapp.gnh.domain.models.LogOutResponse;

public interface LogOutInteractor {
    interface Callback {
        void onLogOutSuccess(LogOutResponse logOutResponse);
        void onLogOutFail(String errorMsg);
    }
}
