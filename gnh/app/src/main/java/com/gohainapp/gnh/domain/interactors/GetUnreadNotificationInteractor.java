package com.gohainapp.gnh.domain.interactors;

public interface GetUnreadNotificationInteractor {
    interface Callback {
        void onGettingUnreadNotificationSuccess(int total);
        void onGettingUnreadNotificationFail(String errorMsg);
    }
}
