package com.gohainapp.gnh.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginResponse {

    @SerializedName("status")
    @Expose
    public int status;

    @SerializedName("access_token")
    @Expose
    public String access_token;

    @SerializedName("expires_in")
    @Expose
    public int expires_in;

    @SerializedName("scope")
    @Expose
    public String scope;

    @SerializedName("refresh_token")
    @Expose
    public String refresh_token;

    @SerializedName("error")
    @Expose
    public String error = null;

    @SerializedName("error_description")
    @Expose
    public String error_description = null;

}
