package com.gohainapp.gnh.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RequestLabMedicineHistoryResponse {
    @SerializedName("data")
    @Expose
    public RequestLabMedicineHistoryData[] requestLabMedicineHistoryData;

    @SerializedName("error")
    @Expose
    public String error;

    @SerializedName("error_description")
    @Expose
    public String error_description;
}
