package com.gohainapp.gnh.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.gohainapp.gnh.R;
import com.gohainapp.gnh.domain.models.NotificationResponseData;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

public class  NotificationListRecyclerViewAdapter extends RecyclerView.Adapter<NotificationListRecyclerViewAdapter.ViewHolder> {

    Context mContext;
    NotificationResponseData[] notificationResponseData;

    public NotificationListRecyclerViewAdapter(Context mContext, NotificationResponseData[] notificationResponseData) {
        this.mContext = mContext;
        this.notificationResponseData = notificationResponseData;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycle_view_message_list_item, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.txtViewDescription.setText(notificationResponseData[position].message);
        if(notificationResponseData[position].created_date != null)
        {
            holder.txtViewTime.setText(convertDate(notificationResponseData[position].created_date));
        }
    }

    @Override
    public int getItemCount() {
        return notificationResponseData.length;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_title)
        TextView txtViewTitle;
        @BindView(R.id.txt_view_description)
        TextView txtViewDescription;
        @BindView(R.id.txt_view_time)
        TextView txtViewTime;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void updateData(NotificationResponseData[] notificationResponseData) {
        this.notificationResponseData = notificationResponseData;
        notifyDataSetChanged();
    }

    private String convertDate(String date)
    {
        SimpleDateFormat spf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date newDate= null;
        try {
            newDate = spf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        spf= new SimpleDateFormat("EEE, HH:mm a");
        date = spf.format(newDate);
        return date;
    }
}
