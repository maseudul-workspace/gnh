package com.gohainapp.gnh.domain.interactors;

public interface VerifyPaymentInteractor {
    interface Callback {
        void onVerifyPaymentSuccess();
        void onVerifyPaymentFailed(String errorMsg);
    }
}
