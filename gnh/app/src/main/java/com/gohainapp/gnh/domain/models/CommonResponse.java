package com.gohainapp.gnh.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CommonResponse {

    @SerializedName("status")
    @Expose
    public int status;

    @SerializedName("messages")
    @Expose
    public Messages messages;

    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("error")
    @Expose
    public int error;

}
