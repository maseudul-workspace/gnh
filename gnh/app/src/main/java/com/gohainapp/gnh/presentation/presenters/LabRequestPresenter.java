package com.gohainapp.gnh.presentation.presenters;

public interface LabRequestPresenter {
    void sendBookingRequest(int type, String medicine_desc, String filepath);
    interface View {
        void onLabRequestSuccess();
        void showLoader();
        void hideLoader();
        void loginError();
    }
}
