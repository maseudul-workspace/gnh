package com.gohainapp.gnh.presentation.presenters.impl;

import android.content.Context;

import com.gohainapp.gnh.AndroidApplication;
import com.gohainapp.gnh.domain.executors.Executor;
import com.gohainapp.gnh.domain.executors.MainThread;
import com.gohainapp.gnh.domain.interactors.FetchUserProfileInteractor;
import com.gohainapp.gnh.domain.interactors.GetUnreadNotificationInteractor;
import com.gohainapp.gnh.domain.interactors.LogOutInteractor;
import com.gohainapp.gnh.domain.interactors.impl.FetchUserProfileInteractorImpl;
import com.gohainapp.gnh.domain.interactors.impl.GetUnreadNotificationInteractorImpl;
import com.gohainapp.gnh.domain.interactors.impl.LogOutInteractorImpl;
import com.gohainapp.gnh.domain.models.LogOutResponse;
import com.gohainapp.gnh.domain.models.LoginResponse;
import com.gohainapp.gnh.domain.models.UserInfoWrapper;
import com.gohainapp.gnh.presentation.presenters.BaseActivityPresenter;
import com.gohainapp.gnh.presentation.presenters.base.AbstractPresenter;
import com.gohainapp.gnh.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class BaseActivityPresenterImpl extends AbstractPresenter implements BaseActivityPresenter,
         FetchUserProfileInteractor.Callback, GetUnreadNotificationInteractor.Callback, LogOutInteractor.Callback

{
    Context mContext;
    View mView;

    public BaseActivityPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void Logout() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        LoginResponse userInfo = androidApplication.getUserInfo(mContext);
        LogOutInteractorImpl logOutInteractorImpl = new LogOutInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.access_token);
        logOutInteractorImpl.execute();
        mView.showLoader();
    }

    @Override
    public void fetchUserDetails() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        LoginResponse userInfo = androidApplication.getUserInfo(mContext);
        FetchUserProfileInteractorImpl fetchUserProfileInteractorImpl = new FetchUserProfileInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.access_token);
        fetchUserProfileInteractorImpl.execute();
        mView.showLoader();
    }

    @Override
    public void fetchNewNotification() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        LoginResponse userInfo = androidApplication.getUserInfo(mContext);
        GetUnreadNotificationInteractorImpl fetchNotificationInteractor = new GetUnreadNotificationInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.access_token);
        fetchNotificationInteractor.execute();
    }

    @Override
    public void onUserProfileFetchSuccess(UserInfoWrapper userInfoWrapper) {
        mView.hideLoader();
        mView.loadUserDetails(userInfoWrapper);
    }

    @Override
    public void onUserProfileFetchFail(String errorMsg) {
        mView.hideLoader();
        if(errorMsg.equals("invalid_token")) {
            mView.loginError();
            AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
            androidApplication.setUserInfo(mContext, null);
        } else {
            Toasty.warning(mContext, errorMsg).show();
        }
    }

    @Override
    public void onGettingUnreadNotificationSuccess(int total) {
        mView.loadNewNotificaction(total);
    }

    @Override
    public void onGettingUnreadNotificationFail(String errorMsg) {
        mView.loadNewNotificaction(0);
    }

    @Override
    public void onLogOutSuccess(LogOutResponse logOutResponse) {
        mView.hideLoader();
        mView.onLogoutSuccess();
    }

    @Override
    public void onLogOutFail(String errorMsg) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }
}
