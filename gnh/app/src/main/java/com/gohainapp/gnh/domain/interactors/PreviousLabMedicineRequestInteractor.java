package com.gohainapp.gnh.domain.interactors;

import com.gohainapp.gnh.domain.models.RequestLabMedicineHistoryData;

public interface PreviousLabMedicineRequestInteractor {
    interface Callback {
        void onPreviousLabMedicineRequestFetchSuccess(RequestLabMedicineHistoryData[] requestLabMedicineHistories);
        void onPreviousLabMedicineRequestFetchFail(String errorMsg);
    }
}
