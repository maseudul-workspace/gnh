package com.gohainapp.gnh.presentation.ui.adapters;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.gohainapp.gnh.presentation.ui.fragment.LabHistoryTabFragment;
import com.gohainapp.gnh.presentation.ui.fragment.MedicineHistoryTabFragment;
import com.gohainapp.gnh.presentation.ui.fragment.VideoHistoryTabFragment;

public class OrderHistoryFragmentAdapter extends FragmentPagerAdapter {

    public OrderHistoryFragmentAdapter(@NonNull FragmentManager fm) {
        super(fm);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {

        Fragment fragment = null;
        if (position == 0)
            fragment = new VideoHistoryTabFragment();
        else if (position == 1)
            fragment = new MedicineHistoryTabFragment();
        else if (position == 2)
            fragment = new LabHistoryTabFragment();

        return fragment;

    }

    @Override
    public int getCount() {
        return 3;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        String title = null;
        if (position == 0)
            title = "Video";
        else if (position == 1)
            title = "Medicine";
        else if (position == 2)
            title = "Lab";
        return title;
    }
}
