package com.gohainapp.gnh.presentation.presenters;

import com.gohainapp.gnh.presentation.ui.adapters.NotificationListRecyclerViewAdapter;

public interface NotificatonPresenter {
    void fetchNotifications(int offset, int limit);
    interface View {
        void loadNotificationAdapter(NotificationListRecyclerViewAdapter adapter, int total);
        void showLoader();
        void hideLoader();
        void loginError();
    }
}
