package com.gohainapp.gnh.presentation.presenters.impl;

import android.content.Context;

import com.gohainapp.gnh.AndroidApplication;
import com.gohainapp.gnh.domain.executors.Executor;
import com.gohainapp.gnh.domain.executors.MainThread;
import com.gohainapp.gnh.domain.interactors.FetchUserProfileInteractor;
import com.gohainapp.gnh.domain.interactors.RegistrationInteractor;
import com.gohainapp.gnh.domain.interactors.UpdateUserProfileInteractor;
import com.gohainapp.gnh.domain.interactors.impl.FetchUserProfileInteractorImpl;
import com.gohainapp.gnh.domain.interactors.impl.RegistrationInteractorImpl;
import com.gohainapp.gnh.domain.interactors.impl.UpdateUserProfileInteractorImpl;
import com.gohainapp.gnh.domain.models.LoginResponse;
import com.gohainapp.gnh.domain.models.UserInfoWrapper;
import com.gohainapp.gnh.presentation.presenters.RegistrationActivityPresenter;
import com.gohainapp.gnh.presentation.presenters.base.AbstractPresenter;
import com.gohainapp.gnh.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class RegistrationActivityPresenterImpl extends AbstractPresenter implements RegistrationActivityPresenter,
        RegistrationInteractor.Callback, FetchUserProfileInteractor.Callback, UpdateUserProfileInteractor.Callback
{
    Context mContext;
    RegistrationActivityPresenter.View mView;

    public RegistrationActivityPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void onRegistrationSuccess(UserInfoWrapper userInfo) {
        mView.hideLoader();
        mView.onRegistrationSuccess();
    }

    @Override
    public void onRegistrationFail(String errorMsg) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }

    @Override
    public void registration(String firstname, String lastname, String phone, String password, String password_confirm, String pinCode, String address, String email) {
        RegistrationInteractorImpl registrationInteractorimpl = new RegistrationInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, firstname, lastname, phone, email, password, password_confirm, pinCode, address);
        registrationInteractorimpl.execute();
        mView.showLoader();
    }

    @Override
    public void update(String userId,String firstname, String lastname, String pinCode, String address, String email, String filepath) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        LoginResponse userInfo = androidApplication.getUserInfo(mContext);
        UpdateUserProfileInteractorImpl updateUserProfileInteractorImpl = new UpdateUserProfileInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.access_token, userId, email, firstname, lastname, address, pinCode, filepath);
        updateUserProfileInteractorImpl.execute();
        mView.showLoader();
    }

    @Override
    public void fetchUserDetails() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        LoginResponse userInfo = androidApplication.getUserInfo(mContext);
        FetchUserProfileInteractorImpl fetchUserProfileInteractorImpl = new FetchUserProfileInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.access_token);
        fetchUserProfileInteractorImpl.execute();
        mView.showLoader();
    }

    @Override
    public void onUserProfileFetchSuccess(UserInfoWrapper userInfoWrapper) {
        mView.hideLoader();
        mView.loadUserDetails(userInfoWrapper);
    }

    @Override
    public void onUserProfileFetchFail(String errorMsg) {
        mView.hideLoader();
        if(errorMsg.equals("invalid_token")) {
            mView.loginError();
            AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
            androidApplication.setUserInfo(mContext, null);
        } else {
            Toasty.warning(mContext, errorMsg).show();
        }
    }

    @Override
    public void onUpdateProfileSuccess(UserInfoWrapper userInfoWrapper) {
        mView.hideLoader();
        mView.onUpdateSuccess();
    }

    @Override
    public void onUpdateProfileFail(String errorMsg) {
        mView.hideLoader();
        if(errorMsg.equals("invalid_token")) {
            mView.loginError();
            AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
            androidApplication.setUserInfo(mContext, null);
        } else {
            Toasty.warning(mContext, errorMsg).show();
        }
    }
}
