package com.gohainapp.gnh.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RequestVideoHistoryData {
    @SerializedName("id")
    @Expose
    public String id;

    @SerializedName("user_id")
    @Expose
    public String user_id;

    @SerializedName("doctor_id")
    @Expose
    public String doctor_id;

    @SerializedName("appointment_time")
    @Expose
    public String appointment_time;

    @SerializedName("amount")
    @Expose
    public String amount;

    @SerializedName("status")
    @Expose
    public String status;

    @SerializedName("approved_by")
    @Expose
    public String approved_by;

    @SerializedName("crated_date")
    @Expose
    public String crated_date;

    @SerializedName("approved_date")
    @Expose
    public String approved_date;

    @SerializedName("payment_date")
    @Expose
    public String payment_date;

    @SerializedName("payment_gateway_id")
    @Expose
    public String payment_gateway_id;

    @SerializedName("payment_link")
    @Expose
    public String payment_link;

    @SerializedName("prescription")
    @Expose
    public String prescription;

    @SerializedName("doctor_name")
    @Expose
    public String doctor_name;
}
