package com.gohainapp.gnh.domain.interactors;

import com.gohainapp.gnh.domain.models.RequestVideoHistoryData;

public interface PreviousVideoRequestInteractor {
    interface Callback {
        void onPreviousVideoRequestFetchSuccess(RequestVideoHistoryData[] requestVideoHistories);
        void onPreviousVideoRequestFetchFail(String errorMsg);
    }
}
