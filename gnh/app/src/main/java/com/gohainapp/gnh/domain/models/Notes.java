package com.gohainapp.gnh.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Notes {
    @SerializedName("address")
    @Expose
    public String address;

    @SerializedName("merchant_order_id")
    @Expose
    public int merchant_order_id;
}
