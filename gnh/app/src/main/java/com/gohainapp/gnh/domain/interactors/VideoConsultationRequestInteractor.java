package com.gohainapp.gnh.domain.interactors;

import com.gohainapp.gnh.domain.models.VideoConsultationRequestResponse;

public interface VideoConsultationRequestInteractor {
    interface Callback {
        void onVideoConsultationRequestSuccess(VideoConsultationRequestResponse videoConsultationRequestResponse);
        void onVideoConsultationRequestFail(String errorMsg);
    }
}
