package com.gohainapp.gnh.presentation.presenters.impl;

import android.content.Context;

import com.gohainapp.gnh.AndroidApplication;
import com.gohainapp.gnh.domain.executors.Executor;
import com.gohainapp.gnh.domain.executors.MainThread;
import com.gohainapp.gnh.domain.interactors.LabMedicineRequestInteractor;
import com.gohainapp.gnh.domain.interactors.impl.LabMedicineRequestInteractorImpl;
import com.gohainapp.gnh.domain.models.LabMedicineRequestResponse;
import com.gohainapp.gnh.domain.models.LoginResponse;
import com.gohainapp.gnh.presentation.presenters.MedicineRequestPresenter;
import com.gohainapp.gnh.presentation.presenters.base.AbstractPresenter;
import com.gohainapp.gnh.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class MedicineRequestPresenterImpl extends AbstractPresenter implements MedicineRequestPresenter, LabMedicineRequestInteractor.Callback {

    Context mContext;
    View mView;

    public MedicineRequestPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void onLabMedicineRequestSuccess(LabMedicineRequestResponse labMedicineRequestResponse) {
        mView.hideLoader();
        mView.onMedicineRequestSuccess();
    }

    @Override
    public void onLabMedicineRequestFail(String errorMsg) {
        mView.hideLoader();
        if(errorMsg.equals("invalid_token")) {
            mView.loginError();
            AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
            androidApplication.setUserInfo(mContext, null);
        } else {
            Toasty.warning(mContext, errorMsg).show();
        }
    }

    @Override
    public void sendBookingRequest(int type, String medicine_desc, String filepath) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        LoginResponse userInfo = androidApplication.getUserInfo(mContext);
        LabMedicineRequestInteractorImpl labMedicineRequestInteractorImpl = new LabMedicineRequestInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.access_token, type, medicine_desc, filepath);
        labMedicineRequestInteractorImpl.execute();
        mView.showLoader();
    }
}
