package com.gohainapp.gnh.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DoctorsList {
    @SerializedName("id")
    @Expose
    public String id;

    @SerializedName("designation")
    @Expose
    public String designation;

    @SerializedName("doctor_name")
    @Expose
    public String doctor_name;

    @SerializedName("rate")
    @Expose
    public String rate;

    @SerializedName("mobile_no")
    @Expose
    public String mobile_no;

    @SerializedName("email")
    @Expose
    public String email;
}
