package com.gohainapp.gnh;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.gohainapp.gnh.domain.models.LoginResponse;
import com.google.gson.Gson;


public class AndroidApplication extends Application {

    LoginResponse userInfo;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public void setUserInfo(Context context, LoginResponse userInfo){
        this.userInfo = userInfo;
        SharedPreferences sharedPref = context.getSharedPreferences(
                "GNH", Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();

        if(userInfo != null) {
            editor.putString("USER", new Gson().toJson(userInfo));
        } else {
            editor.putString("USER", "");
        }

        editor.commit();
    }

    public LoginResponse getUserInfo(Context context){
        LoginResponse user;
        if(userInfo != null){
            user = userInfo;
        }else{
            SharedPreferences sharedPref = context.getSharedPreferences(
                    "GNH", Context.MODE_PRIVATE);
            String userInfoJson = sharedPref.getString("USER","");
            if(userInfoJson.isEmpty()){
                user = null;
            }else{
                try {
                    user = new Gson().fromJson(userInfoJson, LoginResponse.class);
                    this.userInfo = user;
                }catch (Exception e){
                    user = null;
                }
            }
        }
        return user;
    }

}
