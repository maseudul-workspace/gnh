package com.gohainapp.gnh.presentation.presenters;

import com.gohainapp.gnh.domain.models.Banner;

public interface HomeActivityPresenter {
    void fetchBanners();
    interface View {
        void loadBanners(Banner[] banners);
        void showLoader();
        void hideLoader();
        void loginError();
    }
}
