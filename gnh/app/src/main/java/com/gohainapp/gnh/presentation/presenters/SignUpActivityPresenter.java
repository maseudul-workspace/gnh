package com.gohainapp.gnh.presentation.presenters;

public interface SignUpActivityPresenter {
    void generateOtp(String phone);
    void checkOtp(String mobile, String otp);
    interface View {
        void onOtpSendSuccess();
        void onOtpCheckSuccess();
        void showLoader();
        void hideLoader();
    }
}
