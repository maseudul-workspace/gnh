package com.gohainapp.gnh.domain.interactors;

public interface GenerateOtpResetInteractor {
    interface Callback {
        void onGenerateOtpResetSuccess();
        void onGenerateOtpResetFailed(String errorMsg);
    }
}
