package com.gohainapp.gnh.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.gohainapp.gnh.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PaymentSuccessActivity extends AppCompatActivity {

    @BindView(R.id.layout_success)
    View layoutSuccess;
    @BindView(R.id.layout_failure)
    View layoutFailure;
    @BindView(R.id.txt_view_transatcion_no)
    TextView txtViewTransactionNo;
    @BindView(R.id.txt_view_transatcion_date)
    TextView txtViewTransactionDate;
    @BindView(R.id.txt_view_paid_by)
    TextView txtViewPaidBy;
    @BindView(R.id.txt_view_amount)
    TextView txtViewAmount;
    @BindView(R.id.lotte_success)
    LottieAnimationView lottieAnimationViewSuccess ;
    @BindView(R.id.lotte_failure)
    LottieAnimationView lottieAnimationViewFailure ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_success);
        ButterKnife.bind(this);
        setData();
    }

    private void setData() {
        String signature = getIntent().getStringExtra("signature");
        String order_id = getIntent().getStringExtra("order_id");
        String payment_id = getIntent().getStringExtra("payment_id");
        String amount = getIntent().getStringExtra("amount");
        String paid_by = getIntent().getStringExtra("paid_by");
        txtViewTransactionNo.setText(String.format("Transaction No: %s", payment_id));
        boolean is_success = getIntent().getBooleanExtra("is_success", false);
        txtViewPaidBy.setText(paid_by);
        txtViewAmount.setText(String.format("Rs. %s", amount));
        String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
        txtViewTransactionDate.setText(date);
        if (is_success) {
            layoutSuccess.setVisibility(View.VISIBLE);
            lottieAnimationViewSuccess.playAnimation();
        } else {
            layoutFailure.setVisibility(View.VISIBLE);
            lottieAnimationViewFailure.playAnimation();
        }
    }

}