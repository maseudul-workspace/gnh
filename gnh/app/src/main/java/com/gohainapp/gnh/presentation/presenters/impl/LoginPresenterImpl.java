package com.gohainapp.gnh.presentation.presenters.impl;

import android.content.Context;

import com.gohainapp.gnh.AndroidApplication;
import com.gohainapp.gnh.domain.executors.Executor;
import com.gohainapp.gnh.domain.executors.MainThread;
import com.gohainapp.gnh.domain.interactors.CheckLoginIntercator;
import com.gohainapp.gnh.domain.interactors.impl.CheckLoginInteractorImpl;
import com.gohainapp.gnh.domain.models.LoginResponse;
import com.gohainapp.gnh.presentation.presenters.LoginPresenter;
import com.gohainapp.gnh.presentation.presenters.base.AbstractPresenter;
import com.gohainapp.gnh.repository.LoginAppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class LoginPresenterImpl extends AbstractPresenter implements LoginPresenter, CheckLoginIntercator.Callback {

    LoginPresenter.View mView;
    Context mContext;

    public LoginPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void chekLogin(String user, String password) {
        CheckLoginInteractorImpl checkLoginInteractor = new CheckLoginInteractorImpl(mExecutor, mMainThread, new LoginAppRepositoryImpl(), this, "password", user, password);
        checkLoginInteractor.execute();
        mView.showLoader();
    }

    @Override
    public void onLoginSuccess(LoginResponse loginResponse) {
        mView.hideLoader();
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setUserInfo(mContext, loginResponse);
        mView.onLoginSuccess();
    }

    @Override
    public void onLoginFail(String errorMessage) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMessage).show();
    }
}
