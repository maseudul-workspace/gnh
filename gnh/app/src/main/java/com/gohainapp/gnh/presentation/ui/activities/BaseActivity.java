package com.gohainapp.gnh.presentation.ui.activities;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.gohainapp.gnh.AndroidApplication;
import com.gohainapp.gnh.domain.executors.impl.ThreadExecutor;
import com.gohainapp.gnh.domain.models.UserInfoWrapper;
import com.gohainapp.gnh.presentation.presenters.BaseActivityPresenter;
import com.gohainapp.gnh.presentation.presenters.impl.BaseActivityPresenterImpl;
import com.gohainapp.gnh.threading.MainThreadImpl;
import com.gohainapp.gnh.util.GlideHelper;
import com.google.android.material.navigation.NavigationView;

import com.gohainapp.gnh.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BaseActivity extends AppCompatActivity implements BaseActivityPresenter.View {

    @BindView(R.id.toolbar) @Nullable
    Toolbar toolbar;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    TextView textViewUsername;
    ImageView imageViewProfile;
    ActionBarDrawerToggle actionBarDrawerToggle;
    @BindView(R.id.navigation)
    NavigationView navigationView;
    @BindView(R.id.layout_new_notification)
    View layoutNewNotification;
    BaseActivityPresenterImpl mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
    }

    protected void inflateContent(@LayoutRes int inflateResID){
        setContentView(R.layout.activity_base);
        FrameLayout contentFramelayout = (FrameLayout) findViewById(R.id.content_frame);
        getLayoutInflater().inflate(inflateResID, contentFramelayout);
        ButterKnife.bind(this);
        setToolbar();
        setUpNavigationView();
        initialisePresenter();
        mPresenter.fetchUserDetails();
    }

    private void initialisePresenter() {
        mPresenter = new BaseActivityPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setToolbar() {
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.app_name, R.string.app_name);
    }

    public void setUpNavigationView() {
        navigationView.getHeaderView(0).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onProfileClicked();
            }
        });
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                switch (id) {

                    case R.id.nav_video_consultant:
                         drawerLayout.closeDrawer(Gravity.LEFT);
                         goToVideoConsultantActivity();
                         break;

                    case R.id.nav_medicine_booking:
                         drawerLayout.closeDrawer(Gravity.LEFT);
                         goToMedicineBookingActivity();
                         break;

                    case R.id.nav_lab_booking:
                         drawerLayout.closeDrawer(Gravity.LEFT);
                         goToLabTestBookingActivity();
                         break;

                    case R.id.nav_booking_history:
                         drawerLayout.closeDrawer(Gravity.LEFT);
                         goToOrderHistoryActivity();
                         break;

                    case R.id.nav_about:
                         drawerLayout.closeDrawer(Gravity.LEFT);
                         goToAboutActivity();
                         break;

                    case R.id.nav_return_policy:
                        drawerLayout.closeDrawer(Gravity.LEFT);
                        goToReturnPolicyActivity();
                        break;

                    case R.id.nav_terms_conditions:
                         drawerLayout.closeDrawer(Gravity.LEFT);
                         goToTnCActivity();
                         break;

                    case R.id.nav_map:
                        drawerLayout.closeDrawer(Gravity.LEFT);
                        goToMapActivity();
                        break;

                    case R.id.nav_facebook:
                        openFacebookPage();
                        break;

                    case R.id.nav_instagram:
                        openInstagramPage();
                        break;

                    case R.id.nav_twitter:
                        openTwitterPage();
                        break;

                    case R.id.nav_log_out:
                        logOut();
                        break;
                }

                return false;
            }
        });
    }

    @OnClick(R.id.btn_profile) void onProfileClicked () {
        Intent intent = new Intent(this, ProfileSettingsActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.btn_bell) void onNotificationClicked () {
        Intent intent = new Intent(this, NotificationActivity.class);
        startActivity(intent);
    }

    private void goToVideoConsultantActivity() {
        Intent intent = new Intent(this, BookVideoConslutationActivity.class);
        startActivity(intent);
    }

    private void goToMedicineBookingActivity() {
        Intent intent = new Intent(this, RequestMedicineActivity.class);
        startActivity(intent);
    }

    private void goToLabTestBookingActivity() {
        Intent intent = new Intent(this, RequestLabTestActivity.class);
        startActivity(intent);
    }

    private void goToOrderHistoryActivity() {
        Intent intent = new Intent(this, OrderHistoryActivity.class);
        startActivity(intent);
    }

    private void goToTnCActivity() {
        Intent intent = new Intent(this, TermsConditionsActivity.class);
        startActivity(intent);
    }

    private void goToAboutActivity() {
        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
    }

    private void goToReturnPolicyActivity() {
        Intent intent = new Intent(this, ReturnPolicyActivity.class);
        startActivity(intent);
    }

    private void goToMapActivity() {
        Intent intent = new Intent(this, MapActivity.class);
        startActivity(intent);
    }

    private void openTwitterPage() {
        String url = "https://twitter.com/PKMEMORIALNURS1";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    private void openFacebookPage() {
        String url = "https://www.facebook.com/gohainnursinghome/";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    private void openInstagramPage() {
        String url = "https://instagram.com/gohain_nursing_home?utm_medium=copy_link";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    private void logOut() {
        mPresenter.Logout();
    }

    @Override
    public void loadUserDetails(UserInfoWrapper userInfoWrapper) {
        View headerLayout = navigationView.getHeaderView(0);
        textViewUsername = headerLayout.findViewById(R.id.txt_view_header_name);
        imageViewProfile = (ImageView) headerLayout.findViewById(R.id.header_profile_image);
        textViewUsername.setText(capsName(userInfoWrapper.firstname)+""+capsName(userInfoWrapper.lastname));
        if(userInfoWrapper.profile_pic!=null) {
            GlideHelper.setImageViewCustomRoundedCorners(this, imageViewProfile ,getResources().getString(R.string.base_url) + userInfoWrapper.profile_pic,150);
        }
    }

    @Override
    public void loadNewNotificaction(int total) {
        if (total > 0) {
            layoutNewNotification.setVisibility(View.VISIBLE);
        } else {
            layoutNewNotification.setVisibility(View.GONE);
        }
    }

    @Override
    public void onLogoutSuccess() {
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        androidApplication.setUserInfo(getApplicationContext(), null);
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void showLoader() {
    }

    @Override
    public void hideLoader() {
    }

    @Override
    public void loginError() {
        Toast.makeText(this, "Your session has expired !!", Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private StringBuilder capsName(String name)
    {
        StringBuilder res = new StringBuilder();
        try {
            String[] strArr = name.split(" ");
            for (String str : strArr) {
                char[] stringArray = str.trim().toCharArray();
                stringArray[0] = Character.toUpperCase(stringArray[0]);
                str = new String(stringArray);

                res.append(str).append(" ");
            }
            return res;
        } catch (Exception e) {
            return res.append("");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.fetchNewNotification();
    }
}