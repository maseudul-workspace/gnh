package com.gohainapp.gnh.domain.interactors;

import com.gohainapp.gnh.domain.models.UserInfoWrapper;

public interface RegistrationInteractor {
    interface Callback {
        void onRegistrationSuccess(UserInfoWrapper userInfo);
        void onRegistrationFail(String errorMsg);
    }
}
