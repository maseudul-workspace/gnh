package com.gohainapp.gnh.domain.interactors.impl;

import com.gohainapp.gnh.domain.executors.Executor;
import com.gohainapp.gnh.domain.executors.MainThread;
import com.gohainapp.gnh.domain.interactors.DoctorListInteractor;
import com.gohainapp.gnh.domain.interactors.base.AbstractInteractor;
import com.gohainapp.gnh.domain.models.DoctorsList;
import com.gohainapp.gnh.domain.models.DoctorsListResponse;
import com.gohainapp.gnh.repository.AppRepositoryImpl;

public class DoctorListInteractorImpl extends AbstractInteractor implements DoctorListInteractor {

    AppRepositoryImpl mRepository;
    DoctorListInteractor.Callback mCallback;
    String authorization;

    public DoctorListInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String authorization) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.authorization = authorization;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onDoctorsListFetchFail(errorMsg);
            }
        });
    }

    private void postMessage(DoctorsList[] doctors){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onDoctorsListFetchSuccess(doctors);
            }
        });
    }

    @Override
    public void run() {
        final DoctorsListResponse doctorResponse = mRepository.fetchDoctorsList(authorization);
        if (doctorResponse == null) {
            notifyError("Please Check Your Internet Connection");
        } else if(doctorResponse.error != null){
            notifyError("invalid_token");
        } else {
            postMessage(doctorResponse.doctorsList);
        }
    }
}
