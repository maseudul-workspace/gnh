package com.gohainapp.gnh.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.airbnb.lottie.LottieAnimationView;
import com.gohainapp.gnh.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BookingSuccessfullActivity extends AppCompatActivity {

    @BindView(R.id.lotte_success)
    LottieAnimationView lottieAnimationViewSuccess ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_successfull);
        ButterKnife.bind(this);
        lottieAnimationViewSuccess.playAnimation();
    }

    @OnClick(R.id.btn_check_request) void onCheckRequestClicked() {
        Intent intent = new Intent(this, OrderHistoryActivity.class);
        startActivity(intent);
        finish();
    }
}