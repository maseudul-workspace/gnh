package com.gohainapp.gnh.domain.interactors;

import com.gohainapp.gnh.domain.models.UserInfoWrapper;

public interface UpdateUserProfileInteractor {
    interface Callback {
        void onUpdateProfileSuccess(UserInfoWrapper userInfoWrapper);
        void onUpdateProfileFail(String errorMsg);
    }
}
