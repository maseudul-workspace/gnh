package com.gohainapp.gnh.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LabMedicineRequestResponse {

    @SerializedName("status")
    @Expose
    public int status;

    @SerializedName("error")
    @Expose
    public String error;

    @SerializedName("messages")
    @Expose
    public Messages messages;

    @SerializedName("user_id")
    @Expose
    public String user_id;

//    @SerializedName("status")
//    @Expose
//    public String request_status;

    @SerializedName("type")
    @Expose
    public String type;

    @SerializedName("medicine_desc")
    @Expose
    public String medicine_desc;

    @SerializedName("prescription")
    @Expose
    public String prescription;

    @SerializedName("id")
    @Expose
    public int id;

}
