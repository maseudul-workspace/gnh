package com.gohainapp.gnh.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderByRequestResponse {

    @SerializedName("status")
    @Expose
    public int status;

    @SerializedName("messages")
    @Expose
    public Messages messages;

    @SerializedName("error")
    @Expose
    public int error;

    @SerializedName("key")
    @Expose
    public String key;

    @SerializedName("amount")
    @Expose
    public String amount;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("description")
    @Expose
    public String description;

    @SerializedName("image")
    @Expose
    public String image;

    @SerializedName("prefill")
    @Expose
    public Prefill prefill;

    @SerializedName("notes")
    @Expose
    public Notes notes;

    @SerializedName("theme")
    @Expose
    public Theme theme;

    @SerializedName("order_id")
    @Expose
    public String order_id;
}
