package com.gohainapp.gnh.presentation.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;


import com.gohainapp.gnh.R;
import com.gohainapp.gnh.domain.models.RequestVideoHistoryData;
import com.gohainapp.gnh.presentation.ui.activities.PhotoViewActivity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LatestVideoRequestItemsRecyclerViewAdapter extends RecyclerView.Adapter<LatestVideoRequestItemsRecyclerViewAdapter.ViewHolder> {

    Context mContext;
    RequestVideoHistoryData[] requestVideoHistories;
    Callback mCallback;

    public interface Callback {
        void onPaymentClicked(int id, int type);
    }

    public LatestVideoRequestItemsRecyclerViewAdapter(Context mContext, RequestVideoHistoryData[] requestVideoHistories, Callback mCallback) {
        this.mContext = mContext;
        this.requestVideoHistories = requestVideoHistories;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_latest_video_history_items, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
           holder.txtViewDoctorName.setText(requestVideoHistories[position].doctor_name);
           holder.txtViewDate.setText(convertToDate(requestVideoHistories[position].appointment_time));
           holder.txtViewTime.setText(convertToTime(requestVideoHistories[position].appointment_time));
           holder.btnPay.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View view) {
                    mCallback.onPaymentClicked(Integer.parseInt(requestVideoHistories[position].id), 1);
               }
           });

           holder.txtViewRate.setText("Amount: ₹ "+requestVideoHistories[position].amount);

        holder.btnViewPrescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToPhotoViewActivity(mContext.getResources().getString(R.string.base_url) + requestVideoHistories[position].prescription);
            }
        });

        switch (requestVideoHistories[position].status)
        {
            case "1" :
                holder.viewBlue.setVisibility(View.VISIBLE);
                holder.viewGreen.setVisibility(View.GONE);
                holder.viewRed.setVisibility(View.GONE);
                holder.viewYellow.setVisibility(View.GONE);
                holder.btnPay.setVisibility(View.GONE);
                holder.txtViewOrderStatus.setText("Requested");
                break;

            case "2" :
                holder.viewBlue.setVisibility(View.GONE);
                holder.viewGreen.setVisibility(View.VISIBLE);
                holder.viewRed.setVisibility(View.GONE);
                holder.viewYellow.setVisibility(View.GONE);
                holder.txtViewOrderStatus.setText("Accepted");
                break;

            case "4" :
                holder.viewBlue.setVisibility(View.GONE);
                holder.viewGreen.setVisibility(View.GONE);
                holder.viewRed.setVisibility(View.GONE);
                holder.viewYellow.setVisibility(View.VISIBLE);
                holder.txtViewOrderStatus.setText("Payment Pending");
                break;

            case "5" :
                holder.viewBlue.setVisibility(View.GONE);
                holder.viewGreen.setVisibility(View.VISIBLE);
                holder.viewRed.setVisibility(View.GONE);
                holder.viewYellow.setVisibility(View.GONE);
                holder.btnPay.setVisibility(View.GONE);
                holder.txtViewOrderStatus.setText("Paid");
                break;

            case "6" :
                holder.viewBlue.setVisibility(View.GONE);
                holder.viewGreen.setVisibility(View.VISIBLE);
                holder.viewRed.setVisibility(View.GONE);
                holder.viewYellow.setVisibility(View.GONE);
                holder.btnPay.setVisibility(View.GONE);
                holder.txtViewOrderStatus.setText("Prescription Generated");
                holder.btnViewPrescription.setVisibility(View.VISIBLE);
                break;

            case "7" :
                holder.viewBlue.setVisibility(View.GONE);
                holder.viewGreen.setVisibility(View.GONE);
                holder.viewRed.setVisibility(View.VISIBLE);
                holder.viewYellow.setVisibility(View.GONE);
                holder.btnPay.setVisibility(View.GONE);
                holder.txtViewOrderStatus.setText("Payment Failed");
                break;

            case "8" :
                holder.viewBlue.setVisibility(View.GONE);
                holder.viewGreen.setVisibility(View.GONE);
                holder.viewRed.setVisibility(View.VISIBLE);
                holder.viewYellow.setVisibility(View.GONE);
                holder.btnPay.setVisibility(View.GONE);
                holder.txtViewOrderStatus.setText("Rejected");
                break;

            case "9" :
                holder.viewBlue.setVisibility(View.GONE);
                holder.viewGreen.setVisibility(View.VISIBLE);
                holder.viewRed.setVisibility(View.GONE);
                holder.viewYellow.setVisibility(View.GONE);
                holder.btnPay.setVisibility(View.GONE);
                holder.txtViewOrderStatus.setText("Amount Refunded");
                break;

            case "10" :
                holder.viewBlue.setVisibility(View.GONE);
                holder.viewGreen.setVisibility(View.GONE);
                holder.viewRed.setVisibility(View.VISIBLE);
                holder.viewYellow.setVisibility(View.GONE);
                holder.btnPay.setVisibility(View.GONE);
                holder.txtViewOrderStatus.setText("Cancelled");
                break;
        }
    }

    @Override
    public int getItemCount() {
        return requestVideoHistories.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_Name)
        TextView txtViewDoctorName;
        @BindView(R.id.txt_view_Date)
        TextView txtViewDate;
        @BindView(R.id.txt_view_time)
        TextView txtViewTime;
        @BindView(R.id.txt_view_rate)
        TextView txtViewRate;
        @BindView(R.id.text_view_order_status)
        TextView txtViewOrderStatus;
        @BindView(R.id.btn_pay)
        Button btnPay;
        @BindView(R.id.red)
        View viewRed;
        @BindView(R.id.blue)
        View viewBlue;
        @BindView(R.id.green)
        View viewGreen;
        @BindView(R.id.yellow)
        View viewYellow;
        @BindView(R.id.main_layout_class)
        CardView mainLayoutClass;
        @BindView(R.id.btn_view_prescription)
        Button btnViewPrescription;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private String convertToDate(String date)
    {
        SimpleDateFormat spf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date newDate= null;
        try {
            newDate = spf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        spf= new SimpleDateFormat("EEEE, MMM d, yyyy");
        date = spf.format(newDate);
        return date;
    }

    private String convertToTime(String date)
    {
        SimpleDateFormat spf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date newDate= null;
        try {
            newDate = spf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        spf= new SimpleDateFormat("h:mm a");
        date = spf.format(newDate);
        return date;
    }

    //type should be 1
    private void goToPhotoViewActivity (String photoUrl) {
        Intent intent = new Intent(mContext, PhotoViewActivity.class);
        intent.putExtra("photoUrl", photoUrl);
        intent.putExtra("type", 1);
        mContext.startActivity(intent);
    }

}
