package com.gohainapp.gnh.repository;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface AppRepository {

    @GET("user/generate_opt")
    Call<ResponseBody> generateOtp( @Query("mobileNo") String mobile);

    @GET("user/check_opt")
    Call<ResponseBody> checkOtp( @Query("mobileNo") String mobile,
                                 @Query("opt") String otp
    );

    @GET("user/generate_opt_reset")
    Call<ResponseBody> generateOtpReset( @Query("mobileNo") String mobile);

    @POST("user/logout")
    @FormUrlEncoded
    Call<ResponseBody> logOut( @Field("token") String token);

    @POST("user/reset_password")
    @FormUrlEncoded
    Call<ResponseBody> changePassword(
                                       @Field("phone") String phone,
                                       @Field("password") String password,
                                       @Field("password_confirm") String password_confirm

    );

    @POST("user/register")
    @FormUrlEncoded
    Call<ResponseBody> registration(   @Field("firstname") String firstname,
                                       @Field("lastname") String lastname,
                                       @Field("email") String email,
                                       @Field("phone") String phone,
                                       @Field("password") String password,
                                       @Field("password_confirm") String password_confirm,
                                       @Field("address") String address,
                                       @Field("pinCode") String pinCode

    );

    @GET("Doctor/doctorList")
    Call<ResponseBody> fetchDoctorList( @Header("Authorization") String authorization
    );

    @POST("video/create")
    @FormUrlEncoded
    Call<ResponseBody> requestVideoConsultation(   @Header("Authorization") String authorization,
                                                   @Field("appointment_time") String appointmentType,
                                                   @Field("doctor_id") int doctorId
    );

    @GET("video/get_latest_video_user_v1")
    Call<ResponseBody> getLatestVideoRequest( @Header("Authorization") String authorization
    );

    @GET("video/get_previous_video_user_v1")
    Call<ResponseBody> getPreviousVideoRequest( @Header("Authorization") String authorization
    );

    @GET("LabMedicine/get_latest_medicine_labtest_v1")
    Call<ResponseBody> getLatestLabMedicineRequest( @Header("Authorization") String authorization,
                                                    @Query("type") int type
    );

    @GET("LabMedicine/get_previous_medicine_labtest_v1")
    Call<ResponseBody> getPreviousLabMedicineRequest( @Header("Authorization") String authorization,
                                                      @Query("type") int type
    );

    @GET("RazorpayController/get_order_by_request")
    Call<ResponseBody> getOrderByRequest( @Query("id") int id,
                                          @Query("type") int type
    );

    @POST("LabMedicine/create")
    @Multipart
    Call<ResponseBody> requestLabMedicine(  @Header("Authorization") String authorization,
                                            @Part("type") RequestBody type,
                                            @Part("medicine_desc") RequestBody medicine_desc,
                                            @Part MultipartBody.Part prescription
    );

    @GET("user/get_user_details")
    Call<ResponseBody> fetchUserProfile( @Header("Authorization") String authorization
    );

    @GET("banner")
    Call<ResponseBody> fetchBanner( @Header("Authorization") String authorization
    );

    @POST("user/update_user")
    @Multipart
    Call<ResponseBody> profileUpdate(    @Header("Authorization") String authorization,
                                         @Part("user_id") RequestBody userId,
                                         @Part("firstname") RequestBody firstname,
                                         @Part("lastname") RequestBody lastname,
                                         @Part("email") RequestBody email,
                                         @Part("address") RequestBody address,
                                         @Part("pin") RequestBody pinCode,
                                         @Part MultipartBody.Part profile_pic
    );


    @POST("RazorpayController/verify")
    @FormUrlEncoded
    Call<ResponseBody> verifyPayment(   @Header("Authorization") String authorization,
                                        @Field("razorpay_order_id") String razorpayOrderId,
                                        @Field("razorpay_payment_id") String razorpayPaymentId,
                                        @Field("razorpay_signature") String razorpaySignature
    );

    @GET("notification/get_notification_user_page")
    Call<ResponseBody> fetchNotifications(  @Header("Authorization") String authorization,
                                            @Query("offset") int offset,
                                            @Query("limit") int limit
    );

    @GET("notification/get_notification_new_total")
    Call<ResponseBody> getUnreadNotification(  @Header("Authorization") String authorization);

}
