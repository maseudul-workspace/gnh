package com.gohainapp.gnh.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.gohainapp.gnh.R;
import com.gohainapp.gnh.domain.executors.impl.ThreadExecutor;
import com.gohainapp.gnh.domain.models.DoctorsList;
import com.gohainapp.gnh.presentation.presenters.VideoConsultationPresenter;
import com.gohainapp.gnh.presentation.presenters.impl.VideoConsultationPresenterImpl;
import com.gohainapp.gnh.presentation.ui.dialogs.DoctorsListDialog;
import com.gohainapp.gnh.threading.MainThreadImpl;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

public class BookVideoConslutationActivity extends AppCompatActivity implements VideoConsultationPresenter.View, DoctorsListDialog.Callback {


    DoctorsListDialog doctorsListDialog;
    VideoConsultationPresenterImpl mPresenter;
    DoctorsList[] doctors;
    ProgressDialog progressDialog;
    @BindView(R.id.txt_view_select_doctor)
    TextView txtViewSelectDoctor;
    @BindView(R.id.txt_view_select_date)
    TextView txtViewSelectDate;
    @BindView(R.id.txt_view_select_time)
    TextView txtViewSelectTime;
    @BindView(R.id.checkbox_disclaimer)
    CheckBox checkBoxDisclaimer;
    @BindView(R.id.btn_request_doctor)
    Button btnRequestDoctor;
    @BindView(R.id.txt_view_disclaimer)
    TextView txtViewDisclaimer;
    String date;
    String time;
    String month;
    String day;
    String hour;
    String min;
    int doctors_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_video_conslutation);
        ButterKnife.bind(this);
        initialisePresenter();
        initialiseDialogs();
        setProgressDialog();
        checkDisclaimerChecked();
        txtViewDisclaimer.setMovementMethod(new ScrollingMovementMethod());
        mPresenter.fetchDoctors();
    }

    private void checkDisclaimerChecked() {
        checkBoxDisclaimer.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    btnRequestDoctor.setEnabled(true);
                } else {
                    btnRequestDoctor.setEnabled(false);
                }
            }
        });
    }

    private void setProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Its loading....");
        progressDialog.setTitle("Please Wait");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    private void initialisePresenter() {
        mPresenter = new VideoConsultationPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void initialiseDialogs() {
        doctorsListDialog = new DoctorsListDialog(this, this, this);
        doctorsListDialog.setUpDialog();
    }

    @OnClick(R.id.btn_back) void onBackClicked()
    {
        finish();
    }

    @OnClick(R.id.linear_layout_select_doctor) void onSelectDoctorClicked() {
        doctorsListDialog.showDialog();
    }

    @OnClick(R.id.linear_layout_select_date) void onSelectDateClicked() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,

                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        String month = "";
                        if(dayOfMonth<10) {
                            day ="0"+dayOfMonth;
                        } else {
                            day = Integer.toString(dayOfMonth);
                        }


                        if((monthOfYear + 1) < 10) {
                            month ="0"+ (monthOfYear + 1);
                        } else {
                            month = Integer.toString(monthOfYear + 1);
                        }
                        date = year + "-" + month + "-" + day;
                        txtViewSelectDate.setText(date);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    @OnClick(R.id.linear_layout_select_time) void onSelectTimeClicked() {
        // Get Current Time
        final Calendar c = Calendar.getInstance();
        int mHour = c.get(Calendar.HOUR_OF_DAY);
        int mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,

                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {
                        if(hourOfDay == 0) {
                            hour = "12";
                        } else if(hourOfDay<10) {
                            hour ="0"+hourOfDay;
                        } else {
                            hour = Integer.toString(hourOfDay);
                        }

                        if(minute<10) {
                            min = "0"+minute;
                        } else {
                            min = Integer.toString(minute);
                        }
                        time = hour + ":" + min;
                        txtViewSelectTime.setText(time);
                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();
    }

    @Override
    public void loadDoctorsList(DoctorsList[] doctors) {
        this.doctors= doctors;
        doctorsListDialog.setDoctors(doctors);
    }

    @Override
    public void onBookingRequestSuccess() {
        Intent intent = new Intent(this, BookingSuccessfullActivity.class);
        startActivity(intent);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void loginError() {
        Toast.makeText(this, "Your session has expired !!", Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onDoctorSelect(DoctorsList doctorsList) {
        doctorsListDialog.hideDialog();
        txtViewSelectDoctor.setText(doctorsList.doctor_name);
        doctors_id = Integer.parseInt(doctorsList.id);
    }

    @OnClick(R.id.btn_request_doctor) void onRequestClicked() {
        if (doctors_id==0) {
            Toasty.warning(this,"Please select a Doctor before proceeding",Toasty.LENGTH_SHORT).show();
        } else if(date == null || time==null) {
            Toasty.warning(this,"Date or Time cannot be empty",Toasty.LENGTH_SHORT).show();
        } else {
            mPresenter.sendBookingRequest(doctors_id, date+" "+time);
        }
    }
}