package com.gohainapp.gnh.presentation.presenters;

import com.gohainapp.gnh.domain.models.UserInfoWrapper;

public interface ProfileActivitySettingsPresenter {
    void Logout();
    void fetchUserDetails();
    interface View {
        void onLogoutSuccess();
        void loadUserDetails(UserInfoWrapper userInfoWrapper);
        void showLoader();
        void hideLoader();
        void loginError();
    }
}