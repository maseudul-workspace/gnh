package com.gohainapp.gnh.domain.interactors;

import com.gohainapp.gnh.domain.models.OrderByRequestResponse;

public interface OrderByRequestInteractor {
    interface Callback {
        void onGetOrderByRequestSuccess(OrderByRequestResponse orderByRequestResponse);
        void onGetOrderByRequestFail(String errorMsg);
    }
}
